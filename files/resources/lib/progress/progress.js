/* Copyright (c) 2012 by Progress Software Corporation       */
/*                                                           */
/* All rights reserved.  No part of this program or document */
/* may be  reproduced in  any form  or by  any means without */
/* permission in writing from PROGRESS Software Corporation. */

// Version: 11.2.0015
// TODO: Handling of response from POST, PUT and DELETE (merge?)
// TODO: Save idx in index?
// TODO: Remove record (or mark record as deleted) in jsdo.data after remove()
// TODO: Add record to jsdo.data after add()
// TODO: Implement getData()
// TODO: Implement synchronous methods getData()
// TODO: Call validateAddFn()
// TODO: Call validateAssignFn(oldRecord, newRecord)
// TODO: Call validateDeleteFn()
// TODO: Add API for testing
// TODO: Implement batch changes
// TODO: Add validate method?
// TODO: Add JSONP support
// TODO: Optimize memory usage by removing duplicate strings

/*
 * progress.js
 */


(function () {
		
var PROGRESS_JSDO_PCT_MAX_EMPTY_BLOCKS = 20;

/* CRUD-LI */
var PROGRESS_JSDO_OP_NONE 	= 0,
	PROGRESS_JSDO_OP_CREATE = 1,
	PROGRESS_JSDO_OP_READ 	= 2,
	PROGRESS_JSDO_OP_UPDATE = 3,
	PROGRESS_JSDO_OP_DELETE = 4;
	/* PROGRESS_JSDO_OP_INVOKE = 5; */

progress = {};
progress.data = {};
progress.data.nextid = 0;

progress.data.JSDOManager = {};
progress.data.JSDOManager._services = [];    
progress.data.JSDOManager._resources = [];
progress.data.JSDOManager._data = [];
progress.data.JSDOManager._sessions = []; 
progress.data.JSDOManager.put = function(id, jsdo) {
    progress.data.JSDOManager._data[id] = jsdo;    
};
progress.data.JSDOManager.get = function(id) {
    return progress.data.JSDOManager._data[id];
};
progress.data.JSDOManager.addResource = function(id, resource) {
    progress.data.JSDOManager._resources[id] = resource;    
};
progress.data.JSDOManager.getResource = function(id) {
    return progress.data.JSDOManager._resources[id];
};
progress.data.JSDOManager.addService = function(id, service) {
    progress.data.JSDOManager._services[id] = service;    
};
progress.data.JSDOManager.getService = function(id) {
    return progress.data.JSDOManager._services[id];
};
progress.data.JSDOManager.addSession = function(id, session) {
    progress.data.JSDOManager._sessions[id] = session;    
};
progress.data.JSDOManager.getSession = function(id) {
	try {
		return progress.data.JSDOManager._sessions[id];
	}
	catch( e ) {
		return null;
	}
};

/*
 * Scans URI for parameters of the form {name}
 * Returns array with the names
 */
function extractParamsFromURL(url) {
	var urlParams = [];
	if (typeof(url) == 'string') {
		var paramName = null;
		for (var i = 0; i < url.length; i++) {
			if (url.charAt(i) == '{') {
				paramName = "";
			}
			else if (url.charAt(i) == '}') {
				if (paramName)
					urlParams.push(paramName);
				paramName = null;
			}
			else if (paramName != null) {
				paramName += url.charAt(i);
			}
		}
	} 
	return urlParams;
}

/*
 * Adds the catalog.json file provided by the catalog parameter, which is a JSDO 
 * that has loaded the catalog
 */
progress.data.JSDOManager.addCatalog = function( catalog ) {
	var services = catalog.getData()[0].services;
	if (!services) {
		throw new Error( 'Cannot find services property in catalog file' );
	}	
	if (services instanceof Array) {
		// TODO: Register services
		for (var j=0;j<services.length;j++){
			this.addService( services[j].name, services[j] );               // Register the service
			var resources = services[j].resources;
			var baseAddress = services[j].address;
			if (resources instanceof Array) {
				for (var i=0;i<resources.length;i++) {
					var resource = resources[i];
					resource.fn = [];
					resource.service = services[j];       
					resources[i].url = baseAddress + resources[i].path;
					// Register resource 
					progress.data.JSDOManager.addResource(resources[i].name, resources[i]);

					// Process schema
					resource.fields = null;
					if (resource.schema) {
						resource.fields = {};
						// dataProperty is now specified in catalog
						//resource.dataProperty = null;
						resource.dataSetName = null;
						var properties = null;

						try {
							if (typeof resource.schema.properties != 'undefined') {
								var keys = Object.keys(resource.schema.properties);
								properties = resource.schema.properties;
								if (keys.length == 1) {
									if (typeof resource.schema.properties[keys[0]].properties != 'undefined') {
										// Schema corresponds to a DataSet
										resource.dataSetName = keys[0];
									}
									else if (typeof resource.schema.properties[keys[0]].items != 'undefined') {
										// Schema corresponds to a temp-table
										resource.dataProperty = keys[0];
										properties = resource.schema.properties[keys[0]].items.properties;
									}
								}
							}
							else {
								var keys = Object.keys(resource.schema);
								if (keys.length == 1) {
									resource.dataProperty = keys[0];
									if (typeof resource.schema[keys[0]].items != 'undefined') {
										// Catalog format correspond to Table Schema
										properties = resource.schema[keys[0]].items.properties;
									}
									else if (typeof resource.schema[keys[0]].properties != 'undefined') {
										// Catalog format correspond to DataSet Schema
										resource.dataSetName = keys[0];
										resource.dataProperty = null;
										properties = resource.schema;
									}
								}
							}
						}
						catch(e) {
							throw new Error("JSDO: Error parsing catalog file.");
						}
						if (properties) {
							if (resource.dataSetName) {
								properties = properties[resource.dataSetName].properties;
								for (var tableName in properties) {
									resource.fields[tableName] = [];
									var tableProperties;
									if (properties[tableName].items
										&& properties[tableName].items.properties) {
										tableProperties = properties[tableName].items.properties;
									}
									else {
										tableProperties = properties[tableName].properties;
									}
									for (var field in tableProperties) {
										tableProperties[field].name = field;
										resource.fields[tableName].push(tableProperties[field]);	
									}
								}
							}
							else {
								var tableName = resource.dataProperty?resource.dataProperty:"";
								resource.fields[tableName] = [];
								for (var field in properties) {
									properties[field].name = field;
									resource.fields[tableName].push(properties[field]);	
								}
							}
						}
						else
							throw new Error("JSDO: Error parsing catalog file.");
					}
					else
						resource.fields = null;

					// Process operations
					resource.generic = {};
					if (resource.operations) {
						for (var idx=0;idx<resource.operations.length;idx++){
							if (resource.operations[idx].path) {
								resource.operations[idx].url = baseAddress + resource.operations[idx].path;
							}
							else {
								resource.operations[idx].url = baseAddress + resource.path;
							}
							if (!resource.operations[idx].type) {
								resource.operations[idx].type = "INVOKE";
							}
							if (!resource.operations[idx].verb) {
								resource.operations[idx].verb = "PUT";
							}
							if (!resource.operations[idx].params) {
								resource.operations[idx].params = [];
							}

							// Point fn to operations
							var name = resource.operations[idx].name;
                            resource.fn[name] = {};
                            // resource.fn[name].definition = resource.operations[idx];
                            resource.fn[name]["function"] = function fn(object, onCompleteFn, onSuccessFn, onErrorFn) {
                                // Add static variable fnName to function
                                if (typeof fn.fnName == 'undefined') {
                                    fn.fnName = arguments[0]; // Name of function
                                    fn.definition = arguments[1]; // Operation definition
                                    return;
                                }
								var reqBody = null;
                                var url = fn.definition.url;
								var jsdo = this;
								var xhr = null;
							
								if (object) {
									if (typeof(object) != "object") {
										console.error("JSDO: Function '" + fn.fnName + "' requires an object as a parameter.");
										return null;
									}
									var objParam;
									if (object instanceof XMLHttpRequest) {
										jsdo = object.jsdo;
										xhr = object;
										objParam = xhr.objParam;
									}
									else
										objParam = object;

									// Process objParam
									for (var i=0;i<fn.definition.params.length;i++) {
										var name = fn.definition.params[i].name;
										switch (fn.definition.params[i].type) {
										case 'PATH':
										case 'QUERY':
										case 'MATRIX':
											var value = null;
											if (objParam)
 												value = objParam[name];
											if (!value)
												value = "";
											url = url.replace(
													new RegExp('{' + name + '}', 'g'), 
														encodeURIComponent(value));
											break;
										case 'REQUEST_BODY':
											if (xhr && !reqBody) {
												reqBody = objParam;
											}
											else {
												if (!reqBody) {
													reqBody = {};
												}
												reqBody[name] = objParam[name];
											}
											break;
										case 'RESPONSE_BODY':
											break;
										default:
											throw new Error("JSDO Error: Unexpected parameter type '" + fn.definition.params[i].type + "'");
										}
									}
									
									// URL has parameters
									if (url.indexOf('{') != -1) {
										var paramsFromURL = extractParamsFromURL(url);
										for (var i=0; i<paramsFromURL.length;i++) {
											var name = paramsFromURL[i];
											var value = null;
											if (objParam)
												value = objParam[name];
											if (!value)
												value = "";
											url = url.replace(
												new RegExp('{' + name + '}', 'g'), 
													encodeURIComponent(value));
										}
									}
								}

                                var data = jsdo.httpRequest(xhr ? xhr : jsdo, fn.definition.verb, url, reqBody, onCompleteFn, onSuccessFn, onErrorFn);
                                return data;
                            };

							switch(resource.operations[idx].verb.toLowerCase()) {
							case 'get':
							case 'post':
							case 'put':
							case 'delete':
								break;
							default:
								throw new Error("JSDO Error: Unexpected HTTP verb '" + resource.operations[idx].verb + "' found while parsing the catalog.");
							}

							var opname = resource.operations[idx].type.toLowerCase();
							switch (opname) {
							case 'invoke':
								break;
							case 'create':
							case 'read':
							case 'update':
							case 'delete':
								if (typeof(resource.generic[opname]) == "function") {
									throw new Error("JSDO Error: Multiple '" + resource.operations[idx].type + "' operations specified in the catalog for resource '" + resource.name + "'.");
								}
								else
									resource.generic[opname] = resource.fn[name]["function"];
								break;
							default:
								throw new Error("JSDO Error: Unexpected operation '" + resource.operations[idx].type + "' found while parsing the catalog.");
							}

                            // Set fnName
                            resource.fn[name]["function"](name, resource.operations[idx]);
						}
					}
				}
			}
		}
	}

};

/*
 * Prints debug information about the JSDOManager. 
 */
progress.data.JSDOManager.printDebugInfo = function(resourceName) {
	if (resourceName) {
		//console.log("** JSDOManager **");
		//console.log("** BEGIN **");
		var resource = progress.data.JSDOManager.getResource(resourceName);
		if (resource) {
			var cSchema = "Schema:\n";
			var cOperations = "Operations: " + resource.operations.length + "\n";
			for (var field in resource.schema.properties) {
				cSchema += "\nName: " + field
					+	"\n";
			}	

			for (var i=0; i < resource.operations.length; i++) {
				cOperations += "\n" + i
					+	"\nName: " + resource.operations[i].name
					+	"\nURL: " + resource.operations[i].url
					+	"\ntype: " + resource.operations[i].type
					+	"\nverb: " + resource.operations[i].verb
					+	"\nparams: " + resource.operations[i].params.length
					+ 	"\n"; 
			}
			console.log("** DEBUG INFO **\nResource name: %s\nURL:%s\n%s\n%s\n\n",
				resource.name, resource.url, cSchema, cOperations);
		}
		else
			console.log("Resource not found");
		//console.log("** END **");
	}
};

progress.data.JSIndexEntry = function JSIndexEntry(index) {
	this.index = index;
};

progress.data.JSTableRef = function JSTableRef(jsdo, tableName) {
	this._jsdo = jsdo;
	this._name = tableName;
	this._schema = null;
	this._processed = [];

	// Data structure
	this._data = [];
	this._index = [];

	// Arrays to keep track of changes
	this._added = [];
	this._changed = [];
	this._deleted = [];

	this._createIndex = function() {
		this._index = {};
		for (var i = 0; i < this._data.length; i++) {
			var block = this._data[i];
			if (!block) continue;
			this._index[this._data[i].id] = new progress.data.JSIndexEntry(i);
		}
		this._needCompaction = false;
	};

	this._compact = function () {
		var newDataArray = [];
        for (var i = 0; i < this._data.length; i++) {
			var block = this._data[i];
			if (block) {
				newDataArray.push(block);
			}
		}
		this._data = newDataArray;
		this._createIndex();
	};

    this.getData = function () {
		if (this._needCompaction)
			this._compact();
        return this._data;
    };

	// Property: schema
	this.getSchema = function () { return this._schema; };
	this.setSchema = function (schema) { this._schema = schema; };

	// Property: detailPage
	this.getDetailPage = function () { return this._detailPage; };
	this.setDetailPage = function (obj) { this._detailPage = obj; };

	// Property: listview
	this.getListView = function () { return this._listview; };
	this.setListView = function (obj) { this._listview = obj; };

    this.add = function (values) {
        var record = { id: ++progress.data.nextid };
        var schema = this.getSchema();

		// Assign values from the schema
        for(var i = 0; i < schema.length; i++) {
            var fieldName = schema[i].name;
            record[fieldName] = schema[i]["default"];
        }

		// Assign values based on a relationship
		if (this._relationship && this._parent) {
			if (this._jsdo._buffers[this._parent].record) {
				for (var j = 0; j < this._relationship.length; j++) {
					record[this._relationship[j].ChildFieldName] = 
						this._jsdo._buffers[this._parent].record.record[this._relationship[j].ParentFieldName];
				}
			}
		}
		// Assign values from object parameter
        for (var v in values) {
            record[v] = values[v];
        }
        this._data.push(record);
        this._added.push(record.id);
        this._index[record.id] = new progress.data.JSIndexEntry(this._data.length - 1);
		/* TODO: Enable autoSaveChanges
		if (this.tableRef._jsdo.autoSaveChanges)
			this.tableRef._jsdo.saveChanges();
		*/
        return new progress.data.JSRecord(this, record);
    };

    this.findById = function (id) {
		if (id) {
        	var record = this._data[this._index[id].index];
			this.record = record ? (new progress.data.JSRecord(this, record)) : null;
			return this.record;
		}
		else
			return null;
    };

    this.foreach = function (fn) {
		var numEmptyBlocks = 0;
		if (this._needCompaction)
			this._compact();
		this._listviews = [];

		var data = [];

/***
		if (this._isNested && this._parent && this._jsdo._buffers[this._parent]) {
			if (this._jsdo._buffers[this._parent].record
				&& this._jsdo._buffers[this._parent].record.record[this._name]) {
				// Use the array attached to the parent record
				data = this._jsdo._buffers[this._parent].record.record[this._name];
			}
		}
		else 
***/
		if (this._relationship && this._parent) {
			if (this._jsdo._buffers[this._parent].record) {
				// Filter records using relationship
				for (var i = 0; i < this._data.length; i++) {
					var block = this._data[i];
					if (!block) continue;

					var match = false;
					for (var j = 0; j < this._relationship.length; j++) {
						match = (this._jsdo._buffers[this._parent].record.record[this._relationship[j].ParentFieldName] == this._data[i][this._relationship[j].ChildFieldName]);
						if (!match) break;
					}
					if (match)
						data.push(this._data[i]);
				}
			}
		}
		else
			data = this._data;

		this._inforeach = true;
        for (var i = 0; i < data.length; i++) {
			var block = data[i];
			if (!block) {
				numEmptyBlocks++;
				continue;
			}
			this.record = new progress.data.JSRecord(this, data[i]);
            fn(this.record);
        }

		if ((Object.keys(this._listviews) == 0) && this._listview) {
			var listview = this._listview.name;
			$(listview).html('');
			try {
				$(listview).listview("refresh");
			}
			catch(e) {
				// Workaround for issue with JQuery Mobile throwning exception on refresh
			}
		}
		else {
			for (var listview in this._listviews) {
				if (this._listviews[listview])
					$(listview).html(this._listviews[listview]);
				else
					$(listview).html('');
				try {
					$(listview).listview("refresh");
				}
				catch(e) {
					// Workaround for issue with JQuery Mobile throwning exception on refresh
				}
			}
		}
		this._inforeach = false;

		this.record = null;
		if ((numEmptyBlocks * 100 / this._data.length) >= PROGRESS_JSDO_PCT_MAX_EMPTY_BLOCKS)
			jsdo._needCompaction = true;
    };

	this.getFormFields = function () {
		if (!this._schema)
			return '';
		var htmltext = '<input type="hidden" id="id" name="id" value="" />'
                    + '<fieldset data-role="controlgroup">';
        for(var i = 0; i < this._schema.length; i++) {
            var fieldName = this._schema[i].name;
            htmltext += '<div data-role="fieldcontain">'
                    + '<label for="'+fieldName+'">'+fieldName+'</label>'
                    + '<input id="'+fieldName+'" placeholder="" value="" type="text" />'
                    + '</div>';
        }
        return htmltext;
    };
};

/*
 * Returns a JSRecord for the specified JSDO.
 * @param jsdo the JSDO
 * @param record the values of the record
 */
progress.data.JSRecord = function JSRecord(tableRef, record) {
    this.tableRef = tableRef;
    this.record = record;

    this.display = function () {
		var listview;
		var format;
		var detailForm;

		detailForm = this.tableRef._detailPage ? this.tableRef._detailPage : "";
		if (arguments.length == 0
			&& this.tableRef._inforeach 
			&& this.tableRef.getListView()) {

			listview = this.tableRef.getListView().name; 
			format = this.tableRef.getListView().display;
			if (typeof this.tableRef._listviews[listview] == "undefined") {
				// Initialize output of listview htmltext
				this.tableRef._listviews[listview] = ''; 
				// Use JQuery Mobile to clear the listview
				$(listview).html('');
			}
			var text = format;
			for (field in this.record) {
				text = text.replace( new RegExp('{' + field + '}', 'g'), this.record[field]);
			}
			this.tableRef._listviews[listview] += 
						'<li data-theme="c" data-id="' + this.record.id + '">'
                        + '<a href="' + detailForm + '" class="ui-link" data-transition="slide">'
						+ text
                        + '</a></li>';
			return;
		};

		// Display record to listview
		if (arguments.length == 2) {
			if (typeof this.tableRef._listviews[arguments[0]] == "undefined") {
				this.tableRef._listviews[arguments[0]] = ''; // Initialize output of listview htmltext
				$(arguments[0]).html(''); // Use JQuery Mobile to clear the listview
			}
			var text = arguments[1];
			for (field in this.record) {
				text = text.replace( new RegExp('{' + field + '}'), this.record[field]);
			}
			this.tableRef._listviews[arguments[0]] += 
						'<li data-theme="c" data-id="' + this.record.id + '">'
                        + '<a href="' + detailForm + '" class="ui-link" data-transition="slide">'
						+ text
                        + '</a></li>';
			return;
		}

		// Display record to form
        var field;
        var schema = this.tableRef.getSchema();
        for(var i = 0; i < schema.length; i++) {
            var fieldName = schema[i].name;
			if (this.tableRef._detailPage) {
				$(this.tableRef._detailPage + " #" + fieldName).val(this.record[fieldName]);
			}
			else {
				field = document.getElementById(fieldName);
				if (field) {
					field.value = this.record[fieldName];
				}
			}
        }

		if (this.tableRef._detailPage) {
			$(this.tableRef._detailPage + " #id").val(this.record.id);
		}
		else {
			field = document.getElementById('id');
			if (field) {
				field.value = this.record.id;
			} 
		}
    };

	/*
	 * Assigns the specified values or from the screen.
	 * @param record Optional parameter with the record values
	 */
	this.assign = function (record) {
  	var field;
		var fieldName;
		var value;
		var schema = this.tableRef.getSchema();
		if (record) {
			for(var i = 0; i < schema.length; i++) {
				fieldName = schema[i].name;
				value = record[fieldName];
				if (typeof value != "undefined")
			    	this.record[fieldName] = value;
	    }
		}
		else {
			for(var i = 0; i < schema.length; i++) {
				fieldName = schema[i].name;
      	field = document.getElementById(fieldName);
				if (field)
					this.record[fieldName] = field.value;
			}
		}
		this.tableRef._changed.push(this.record.id);
		if (this.tableRef._jsdo.autoSaveChanges)
			this.tableRef._jsdo.saveChanges();
	};

    this.remove = function () {
        var index = this.tableRef._index[this.record.id].index;
		var jsrecord = this.tableRef.findById(this.record.id);
        this.tableRef._deleted.push(jsrecord);
		// Set entry to null instead of removing entry - index requires positions to be persistent
		this.tableRef._data[index] = null;
		delete this.tableRef._index[this.record.id];
		if (this.tableRef._jsdo.autoSaveChanges)
			this.tableRef._jsdo.saveChanges();
    };

};

/*
 * Returns a JSDO for the specified resource.
 * @param sessionOrParmObj: the progress.data.Session to use for communicating with the server,
 *                          or an object that contains the initial values for the JSDO
 *                          (if this is an object, it should include the name and _session properties)
 * @param resName : name of resource (ignored if 1st param is an object containing the initial values)
 */
progress.data.JSDO = function JSDO(resNameOrParmObj, serviceName  ) {
	// Initial values
	this.type = "rest";

	this._currentOperation = PROGRESS_JSDO_OP_NONE;

	this._buffers = {}; 		// Object of table references
	this._numBuffers = 0;
	this._defaultTableRef = null;

	this._async = true;
	this.dataProperty = null;
	this.dataSetName = null;
	this.operations = [];
	this._session = null;
	this._needCompaction = false;
	
    // Initialize Session using init values
	if ( !arguments[0] ) {
		throw new Error("JSDO: Parameters are required in constructor.");
	}
	
	if ( typeof(arguments[0]) ==  "string") {
		this.name = arguments[0];
//		if ( arguments[1] && (typeof(arguments[1]) ==  "string") )
//			localServiceName = serviceName;
	}
	else if ( typeof(arguments[0]) == "object" ) {
    	for (var v in arguments[0]) {
    		this[v] = arguments[0][v];
    	}
	}
	/* error out if caller didn't pass the resource name or a URL:
	 * (we only expect name to be null and URL to be used if we are using this
	 *  JSDO to get the catalog (is there a way to enforce that?) ) 
	 */
    if ( (!this.name && !this.url) /*|| !(this._session)*/ ) {
    	// make this error message more specific?
    	throw new Error( "JSDO: JSDO constructor is missing name" );
    }
    
	if (this.name) {
		// Read resource definition from the Catalog - save reference to JSDO
		// TODO: enhance this to deal with multiple services loaded and the same resource
		// name is used by more than one service (use the local serviceName var)
		this.resource = progress.data.JSDOManager.getResource(this.name);
		if (this.resource) {
			if (!this.url)
				this.url = this.resource.url;
			if (!this.dataSetName && this.resource.dataSetName) {
				// Catalog defines a DataSet
				this.dataSetName = this.resource.dataSetName;

				// Define TableRef property in the JSDO
				if (this.resource.dataProperty) {
					var buffer = this[this.resource.dataProperty] 
							   = new progress.data.JSTableRef(this, this.resource.dataProperty);
					this._buffers[this.resource.dataProperty] = buffer;
				}
				else {
					for (var tableName in this.resource.fields) {
						var buffer = this[tableName]
								   = new progress.data.JSTableRef(this, tableName);
						this._buffers[tableName] = buffer;
					}
				}
			}
			if (!this.dataProperty && this.resource.dataProperty)
				this.dataProperty = this.resource.dataProperty;

			if (!this.dataSetName) {
				var tableName = this.dataProperty?this.dataProperty:"";
				this._buffers[tableName] = new progress.data.JSTableRef(this, tableName);
			}

			// Add functions for operations to JSDO object
			for (fnName in this.resource.fn) {
				this[fnName] = this.resource.fn[fnName]["function"];
			}
			/* get a session object, using name of the service to look it up in the list of
			 * sessions maintained by the JSDOManager
			 */
			if ( !this._session ) {
				this._session = progress.data.JSDOManager.getSession( this.resource.service.name );
			}
		}
	}
	else {
		this._buffers[""] = new progress.data.JSTableRef(this, "");
	}

	if ( !this._session) {
		throw new Error("JSDO: Unable to get user session for JSDO");
	}	

	// Calculate _numBuffers and _defaultTableRef
	for (var buf in this._buffers) {
		this._buffers[buf]._parent = null;
		this._buffers[buf]._children = [];
		this._buffers[buf]._relationship = null;
		this._buffers[buf]._isNested = false;
		if (!this._defaultTableRef)
			this._defaultTableRef = this._buffers[buf];
		this._numBuffers++;
	}
	if (this._numBuffers != 1)
		this._defaultTableRef = null;

	// Set schema for TableRef
	if (this.resource && this.resource.fields) {
		for (var buf in this._buffers) {
			this._buffers[buf]._schema = this.resource.fields[buf];
		}
		// Set schema for when dataProperty is used but not specified via the catalog
		if (this._defaultTableRef 
			&& !this._defaultTableRef._schema
			&& this.resource.fields[""]) {
			this._defaultTableRef._schema = this.resource.fields[""];
		}
	}
	else {
		if (this._defaultTableRef)
			this._defaultTableRef._schema = [];
	}

	// Set isNested property
	if (this._numBuffers > 1) {
		for (var buf in this._buffers) {
			var fields = [];
			var found = false;
			for (var i = 0; i < this._buffers[buf]._schema.length; i++) {
				var field = this._buffers[buf]._schema[i];

				if (field.items
					&& field.type == "array" && field.items.$ref) { 
					if (this._buffers[field.name]) {
						found = true;
						this._buffers[field.name]._isNested = true;
					}
				}
				else
					fields.push(field);
			}
			// Replace list of fields - removing nested datasets from schema
			if (found)
				this._buffers[buf]._schema = fields;
		}
	}

	// Process relationships
	if (this.resource && this.resource.relations) {
		for (var i = 0; i < this.resource.relations.length; i++) {
			var relationship = this.resource.relations[i];
			this._buffers[relationship.ChildName]._parent = relationship.ParentName;
			this._buffers[relationship.ChildName]._relationship = relationship.RelationFields;
			this._buffers[relationship.ParentName]._children.push(relationship.ChildName);
		}
	}

	// Functions
	this.isDataSet = function() {
		return this.dataSetName ? true: false;
	};

    this.display = function () {
		if (this.record) {
			// TODO: Pass arguments 
			this.record.display();
		}
	};

	this.onReadyStateChangeInvoke = function () {
		var xhr = this;
		if (xhr.readyState == 4) {
			var jsdo = this.jsdo;

			jsdo.lastHttpStatus = xhr.status;
			if ((xhr.status >= 200 && xhr.status < 300) || xhr.status == 0)
				jsdo.lastResponseText = null;
			else
				jsdo.lastResponseText = xhr.responseText;

			var data = null;
			try {
				data = JSON.parse(xhr.responseText);
			}
			catch(e) {
				data = null;
				jsdo.lastResponseText = xhr.responseText;
			}
			xhr.result = data;
			if ((xhr.status >= 200 && xhr.status < 300) || xhr.status == 0) {
				if ((typeof xhr.onSuccessFn) == 'function')
					xhr.onSuccessFn(xhr);
				else if ((typeof xhr.jsdo.onSuccessFn) == 'function')
					xhr.jsdo.onSuccessFn(xhr);
			}
			else {
				if ((typeof xhr.onErrorFn) == 'function')
					xhr.onErrorFn(xhr);
				else if ((typeof xhr.jsdo.onErrorFn) == 'function')
					xhr.jsdo.onErrorFn(xhr);
			}
			if ((typeof xhr.onCompleteFn) == 'function')
				xhr.onCompleteFn(xhr);
			else if ((typeof xhr.jsdo.onCompleteFn) == 'function')
				xhr.jsdo.onCompleteFn(xhr);
			xhr.jsdo = null;
		}
	};

	/*
	 * Performs an HTTP request using the specified parameters.
	 */
	this.httpRequest = function (object, method, url, reqBody, onCompleteFn, onSuccessFn, onErrorFn) {
		var data = null;
		var xhr;
		var jsdo;
		var async;

		if (object instanceof XMLHttpRequest) {
			xhr = object;
			jsdo = xhr.jsdo;
			// async = true;
			async = jsdo._async; // Use async option from JSDO
			// Do not set up xhr call - use already set up XHR object
		}
		else {
			// Set up xhr call
			xhr = new XMLHttpRequest();
			jsdo = object;
			xhr = new XMLHttpRequest();
			xhr.jsdo = jsdo;
			if (!onCompleteFn && !onSuccessFn && !onErrorFn)  {
				async = false;
			}
			else {
				async = true;
			}
			xhr.onCompleteFn = onCompleteFn;
			xhr.onSuccessFn = onSuccessFn;
			xhr.onErrorFn = onErrorFn;
			xhr.onreadystatechange = this.onReadyStateChangeInvoke;
		}
		
		if (async) {
			this._session._openRequest(xhr, method, url, true); // Asynchronous request
		}
		else {
			this._session._openRequest(xhr, method, url, false); // Synchronous request 
		}

		var input = null;
		if (reqBody) {
			xhr.setRequestHeader("Content-type", "application/json; charset=utf-8");
			input = JSON.stringify(reqBody);
		}
		xhr.send(input);

		if (!async) {
			jsdo.lastHttpStatus = xhr.status;
			if ((xhr.status >= 200 && xhr.status < 300) || xhr.status == 0)
				jsdo.lastResponseText = null;
			else
				jsdo.lastResponseText = xhr.responseText;

			try {
				data = JSON.parse(xhr.responseText);
			}
			catch(e) {
				data = null;
				jsdo.lastResponseText = xhr.responseText;
			}
		}
		return data;
	};

	this.getFormFields = function() {
		if (this._defaultTableRef)
			return this._defaultTableRef.getFormFields();
		throw new Error("JSDO: JSDO has multiple tables use getFormFields() at the table reference level.");
	};

	this.add = function(obj) {
		if (this._defaultTableRef)
			return this._defaultTableRef.add(obj);
		throw new Error("JSDO: JSDO has multiple tables use add() at the table reference level.");
	};

	this.getData = function() {
		if (this._defaultTableRef)
			return this._defaultTableRef.getData();
		throw new Error("JSDO: JSDO has multiple tables use getData() at the table reference level.");
	};

    this.getSchema = function () {
		if (this._defaultTableRef)
			return this._defaultTableRef.getSchema();
		throw new Error("JSDO: JSDO has multiple tables use getSchema() at the table reference level.");
    };

	this.foreach = function(fn) {
		if (this._defaultTableRef)
			return this._defaultTableRef.foreach(fn);
		throw new Error("JSDO: JSDO has multiple tables use foreach() at the table reference level.");
	};

	this.setListView = function(obj) {
		if (this._defaultTableRef)
			return this._defaultTableRef.setListView(obj);
		throw new Error("JSDO: JSDO has multiple tables use setListView() at the table reference level.");
	};

	this.getListView = function() {
		if (this._defaultTableRef)
			return this._defaultTableRef.getListView();
		throw new Error("JSDO: JSDO has multiple tables use getListView() at the table reference level.");
	};

	this.setDetailPage = function(str) {
		if (this._defaultTableRef)
			return this._defaultTableRef.setDetailPage(str);
		throw new Error("JSDO: JSDO has multiple tables use setDetailPage() at the table reference level.");
	};

	this.getDetailPage = function() {
		if (this._defaultTableRef)
			return this._defaultTableRef.getDetailPage();
		throw new Error("JSDO: JSDO has multiple tables use getDetailPage() at the table reference level.");
	};


	/*
	 * Loads data from the HTTP resource.
	 */
	this.fill = function () {
		var objParam, onCompleteFn, onSuccessFn, onErrorFn;

		// Process parameters
		if (arguments[0]) {
			// Call to fill() has parameters
			switch(typeof(arguments[0])) {
			case 'string':
				// fill( string, onCompleteFn, onSuccessFn, onErrorFn);
				objParam = { filterString: arguments[0] };
				onCompleteFn = arguments[1];
				onSuccessFn = arguments[2];
				onErrorFn = arguments[3];
				break;
			case 'function':
				// fill( onCompleteFn, onSuccessFn, onErrorFn);
				objParam = null;
				onCompleteFn = arguments[0];
				onSuccessFn = arguments[1];
				onErrorFn = arguments[2];
				break;
			default:
				throw new Error("JSDO: Invalid parameters in call to fill() function.");
			}
		}
		else {
			objParam = null;
			onCompleteFn = onSuccessFn = onErrorFn = null;
		}

		var xhr = new XMLHttpRequest();
		xhr.jsdo = this;
		xhr.onCompleteFn = onCompleteFn;
		xhr.onSuccessFn = onSuccessFn;
		xhr.onErrorFn = onErrorFn;
		xhr.onreadystatechange = this.onReadyStateChangeGeneric;
		xhr.jsdo._currentOperation = xhr.operation = PROGRESS_JSDO_OP_READ;

		if (this.resource) {
			if (typeof(this.resource.generic.read) == "function") {
				xhr.objParam = objParam;
				this.resource.generic.read(xhr);
			}
			else {
				throw new Error("JSDO: READ operation is not defined.");
			}
		}
		else {
			this._session._openRequest(xhr, 'GET', this.url, this._async);
			xhr.onreadystatechange = null;
			xhr.send(null);
			if (!this._async) {
				// Testing of the JSDO shows that in some cases a synchronous send() would clear
				// the xhr.jsdo property 
				xhr.jsdo = this;
				xhr.onreadystatechange = this.onReadyStateChangeGeneric;
				xhr.onreadystatechange();
			}
		}
	};

	this._execGenericOperation = function(
		operation, objParam, onCompleteFn, onSuccessFn, onErrorFn) {

		var xhr = new XMLHttpRequest();
		xhr.jsdo = this;
		xhr.onCompleteFn = onCompleteFn;
		xhr.onSuccessFn = onSuccessFn;
		xhr.onErrorFn = onErrorFn;
		xhr.onreadystatechange = this.onReadyStateChangeGeneric;
		xhr.jsdo._currentOperation = xhr.operation = operation;

		var operationStr;
		switch(xhr.operation) {
		case PROGRESS_JSDO_OP_READ: 	operationStr = "read"; break;
		case PROGRESS_JSDO_OP_CREATE: 	operationStr = "create"; break;
		case PROGRESS_JSDO_OP_UPDATE: 	operationStr = "update"; break;
		case PROGRESS_JSDO_OP_DELETE: 	operationStr = "delete"; break;
		default:
			throw new Error("JSDO: Unexpected operation " + xhr.operation + " in HTTP request.");
		}

		if (this.resource) {
			if (typeof(this.resource.generic[operationStr]) == "function") {
				xhr.objParam = objParam;
				this.resource.generic[operationStr](xhr);
			}
			else {
				throw new Error("JSDO: " + operationStr.toUpperCase() + " operation is not defined.");
			}
		}
	};

	this.saveChanges = function (onCompleteFn, onSuccessFn, onErrorFn) {
		if (this.dataSetName)
			this._syncDataSet(onCompleteFn, onSuccessFn, onErrorFn);
		else
			this._syncSingleTable(onCompleteFn, onSuccessFn, onErrorFn);
	};

	this._syncDataSet = function(onCompleteFn, onSuccessFn, onErrorFn) {
		var jsonObject = {};
		var execOperation = false;

		// Synchronize deletes
		for (var buf in this._buffers) { jsonObject[buf] = []; }
		for (var buf in this._buffers) {
			var tableRef = this._buffers[buf];
			for (var i = 0; i < tableRef._deleted.length; i++) {
            	var id = tableRef._deleted[i].id;
            	var jsrecord = tableRef._deleted[i];

				if (!jsrecord) continue;
				tableRef._processed[id] = jsrecord.record;

				jsonObject[buf].push(jsrecord.record);
				execOperation = true;
			}
		}
		if (execOperation)
			this._execGenericOperation(
				PROGRESS_JSDO_OP_DELETE, jsonObject, onCompleteFn, onSuccessFn, onErrorFn);

		// Synchronize adds
		execOperation = false;
		for (var buf in this._buffers) { jsonObject[buf] = []; }
		for (var buf in this._buffers) {
			var tableRef = this._buffers[buf];
			for (var i = 0; i < tableRef._added.length; i++) {
            	var id = tableRef._added[i];
				var jsrecord = tableRef.findById(id);

				if (!jsrecord) continue;
				if (tableRef._processed[id]) continue;
				tableRef._processed[id] = jsrecord.record;

				jsonObject[buf].push(jsrecord.record);
				execOperation = true;
			}
		}
		if (execOperation)
			this._execGenericOperation(
				PROGRESS_JSDO_OP_CREATE, jsonObject, onCompleteFn, onSuccessFn, onErrorFn);

		// Synchronize updates
		execOperation = false;
		for (var buf in this._buffers) { jsonObject[buf] = []; }
		for (var buf in this._buffers) {
			var tableRef = this._buffers[buf];
			for (var i = 0; i < tableRef._changed.length; i++) {
				var id = tableRef._changed[i];
				var jsrecord = tableRef.findById(id);

				if (!jsrecord) continue;
				if (tableRef._processed[id]) continue;
				tableRef._processed[id] = jsrecord.record;

				jsonObject[buf].push(jsrecord.record);
				execOperation = true;
			}
		}
		if (execOperation)
			this._execGenericOperation(
				PROGRESS_JSDO_OP_UPDATE, jsonObject, onCompleteFn, onSuccessFn, onErrorFn);

		// TODO: _changed should only be cleared on successful request 
		for (var buf in this._buffers) {
			var tableRef = this._buffers[buf];
			tableRef._processed = [];
			tableRef._added = [];
			tableRef._changed = [];
			tableRef._deleted = [];
		}
	};

	this._syncSingleTable = function(onCompleteFn, onSuccessFn, onErrorFn) {
		if (!this._defaultTableRef) return;
	var tableRef = this._defaultTableRef;
		// Synchronize deletes
  	for(var i = 0; i < tableRef._deleted.length; i++) {
            var id = tableRef._deleted[i].id;
            var jsrecord = tableRef._deleted[i];

            if (!jsrecord) continue;
            tableRef._processed[id] = jsrecord.record;

            var xhr = new XMLHttpRequest();
            xhr.jsdo = this;
            xhr.onCompleteFn = onCompleteFn;
            xhr.onSuccessFn = onSuccessFn;
            xhr.onErrorFn = onErrorFn;
            xhr.onreadystatechange = this.onReadyStateChangeGeneric;
			xhr.jsdo._currentOperation = xhr.operation = PROGRESS_JSDO_OP_DELETE;

			if (this.resource) {
				if (typeof(this.resource.generic["delete"]) == "function") {
					xhr.objParam = jsrecord.record;
					this.resource.generic["delete"](xhr);
				}
				else {
					throw new Error("JSDO: DELETE operation is not defined.");
				}
			}
			else {
            	this._session._openRequest(xhr, 'DELETE', this.url + '/' + id, true);
    	    	xhr.send(null);
			}
		}
		// Synchronize adds
		for(var i = 0; i < tableRef._added.length; i++) {
			var id = tableRef._added[i];
			var jsrecord = tableRef.findById(id);

			if (!jsrecord) continue;
			if (tableRef._processed[id]) continue;
			tableRef._processed[id] = jsrecord.record;

			var xhr = new XMLHttpRequest();
			xhr.jsdo = this;
			xhr.onCompleteFn = onCompleteFn;
			xhr.onSuccessFn = onSuccessFn;
			xhr.onErrorFn = onErrorFn;
			xhr.onreadystatechange = this.onReadyStateChangeGeneric;
			xhr.jsdo._currentOperation = xhr.operation = PROGRESS_JSDO_OP_CREATE;
			xhr.recordId = id;

			if (this.resource) {
				if (typeof(this.resource.generic.create) == "function") {
					xhr.objParam = jsrecord.record;
					this.resource.generic.create(xhr);
				}
				else {
					throw new Error("JSDO: CREATE operation is not defined.");
				}
			}
			else {
				this._session._openRequest(xhr, 'POST', this.url, true);
				xhr.setRequestHeader("Content-type", "application/json; charset=utf-8");
				var input = JSON.stringify(jsrecord.record);
				xhr.send(input);
			}
		}
		// Synchronize updates
		for(var i = 0; i < tableRef._changed.length; i++) {
			var id = tableRef._changed[i];
			var jsrecord = tableRef.findById(id);

			if (!jsrecord) continue;
			if (tableRef._processed[id]) continue;
			tableRef._processed[id] = jsrecord.record;

			var xhr = new XMLHttpRequest();
			xhr.jsdo = this;
			xhr.onCompleteFn = onCompleteFn;
			xhr.onSuccessFn = onSuccessFn;
			xhr.onErrorFn = onErrorFn;
			xhr.onreadystatechange = this.onReadyStateChangeGeneric;
			xhr.jsdo._currentOperation = xhr.operation = PROGRESS_JSDO_OP_UPDATE;
			xhr.recordId = id;

			if (this.resource) {
				if (typeof(this.resource.generic.update) == "function") {
					xhr.objParam = jsrecord.record;
					this.resource.generic.update(xhr);
				}
				else {
					throw new Error("JSDO: UPDATE operation is not defined.");
				}
			}
			else {
				this._session._openRequest(xhr, 'PUT', this.url + '/' + id, this._async);
				xhr.setRequestHeader("Content-type", "application/json; charset=utf-8");
				var input = JSON.stringify(jsrecord.record);
				xhr.send(input);
				if (!this._async) {
					xhr.jsdo = this; // jsdo property is cleared after synchronous send()
					xhr.onreadystatechange();
				}
			}
		}

		// TODO: _changed should only be cleared on successful request 
		tableRef._processed = [];
		tableRef._added = [];
		tableRef._changed = [];
		tableRef._deleted = [];
	};

	this.clear = function () {
	};

    this.findById = function (id) {
		if (this._defaultTableRef)
			return this._defaultTableRef.findById(id);
		throw new Error("JSDO: JSDO has multiple tables use findById() at the table reference level.");
    };

    this.sort = function (fn) {
    };

	this.onReadyStateChangeGeneric = function () {
		var xhr = this;
		if (xhr.readyState == 4) {
			var jsdo = this.jsdo;
			var jsonObject;

			jsdo.lastHttpStatus = xhr.status;  // Session.login() needs this
			try {
				jsonObject = JSON.parse(xhr.responseText);
				if (jsdo._currentOperation == PROGRESS_JSDO_OP_READ) {
					if (jsdo.isDataSet()) {
						if (jsdo.dataSetName) {
							if (jsdo.dataProperty) {
								jsdo._buffers[jsdo.dataProperty]._data
									= jsonObject[jsdo.dataSetName][jsdo.dataProperty];
								jsdo._buffers[jsdo.dataProperty]._createIndex();
							}
							else {
								// Load data from JSON object into _data
								for (var buf in jsdo._buffers) {
									var data = jsonObject[jsdo.dataSetName][buf];
									data = data?data:[];
									jsdo._buffers[buf]._data = data;
									jsdo._buffers[buf]._createIndex();
								}
								// Load nested data into _data
								if (jsdo._numBuffers > 1) {
									for (var buf in jsdo._buffers) {
										if (jsdo._buffers[buf]._isNested
											&& jsdo._buffers[buf]._parent
											&& jsdo._buffers[jsdo._buffers[buf]._parent]) {
											var srcData = jsdo._buffers[jsdo._buffers[buf]._parent]._data;
											var data = [];
											for (var i = 0; i < srcData.length; i++) {
												for (var j = 0; j < srcData[i][buf].length; j++) {
													data.push(srcData[i][buf][j]);
												}
											}
											jsdo._buffers[buf]._data = data;
											jsdo._buffers[buf]._createIndex();
										}
									}
								}
							}
						}
					}
					else {
						if (jsonObject instanceof Array) {
							jsdo._defaultTableRef._data = jsonObject;
						}
						else {
							if (jsdo.dataProperty)
								jsdo._defaultTableRef._data = jsonObject[jsdo.dataProperty];
							else if (jsonObject.data)
								jsdo._defaultTableRef._data = jsonObject.data;
							else {
								jsdo._defaultTableRef._data = [];
								jsdo._defaultTableRef._data[0] = jsonObject;
							}
						}
					}

					for (var buf in jsdo._buffers) {
						jsdo._buffers[buf]._createIndex();
					}
				}
			}
			catch(e) {
				jsonObject = {};
				if (xhr.operation == PROGRESS_JSDO_OP_READ) {
					for (var buf in jsdo._buffers) {
						jsdo._buffers[buf]._data = [];
						jsdo._buffers[buf]._index = {};
					}
				}
			}

			jsonObject.status = xhr.status;
            // xhr.status is zero for requests using the local file system
			if ((xhr.status >= 200 && xhr.status < 300) || xhr.status == 0) {
				switch(xhr.operation) {
				case PROGRESS_JSDO_OP_READ:
					break;
				case PROGRESS_JSDO_OP_CREATE:
				case PROGRESS_JSDO_OP_UPDATE:
					// Update client JSDO with changes from server
					if (jsdo._defaultTableRef) {
						if (jsonObject.data instanceof Array && jsonObject.data.length == 1) {
							var index = jsdo._defaultTableRef._index[xhr.recordId].index;
							var record = jsdo._defaultTableRef._data[index] = jsonObject.data[0]; 
							delete jsdo._defaultTableRef._index[xhr.recordId];
							jsdo._defaultTableRef._index[record.id] = new progress.data.JSIndexEntry(index);
						}
					}
					break;
				case PROGRESS_JSDO_OP_DELETE:
					break;
				default:
					throw new Error("JSDO: Unexpected operation " + xhr.operation + " in HTTP request.");
				}
				if ((typeof xhr.onSuccessFn) == 'function')
					xhr.onSuccessFn(jsonObject);
				else if ((typeof xhr.jsdo.onSuccessFn) == 'function')
					xhr.jsdo.onSuccessFn(jsonObject);
			}
			else {
				if ((typeof xhr.onErrorFn) == 'function')
					xhr.onErrorFn(jsonObject);
				else if ((typeof xhr.jsdo.onErrorFn) == 'function')
					xhr.jsdo.onErrorFn(jsonObject);
			}
			if ((typeof xhr.onCompleteFn) == 'function')
				xhr.onCompleteFn(jsonObject);
			else if ((typeof xhr.jsdo.onCompleteFn) == 'function')
				xhr.jsdo.onCompleteFn(jsonObject);
			xhr.jsdo._currentOperation = PROGRESS_JSDO_OP_NONE;
			xhr.jsdo = null;
		}
	};


	/*
	 * Invokes an operation defined via the catalog.
	 */
	this.invoke = function(fnName, objParam, onCompleteFn, onSuccessFn, onErrorFn) {
		// Process arguments
		if (arguments.length == 0) {
			console.error("JSDO: invoke() function requires parameters.");
			return;
		}

		if (typeof(arguments[0]) != "string") {
			console.error("JSDO: First parameter to invoke() function must be a string.");
			return null;
		}

		if (arguments[1] && typeof(arguments[1]) != "object") {
			console.error("JSDO: Invoke of '" + arguments[0] + "' requires an object as a parameter.");
			return null;
		}


		return this[arguments[0]](objParam, onCompleteFn, onSuccessFn, onErrorFn);
	};

	// Load data
	if (this.autoFill)
		this.fill();

}; // End of JSDO


/*
 * Manages authentication and session ID information for a service.
 * @param catalogURL : either the URL for the catalog, or - 
 *                     an object that contains initial values for properties
 *                  If the latter, the object must include catalogURL and serviceName props 
 * @param serviceName : the name of the service in the catalog
 *                  (if we support more than one service in a catalog, this will 
 *                   designate the service to load)
 *                  (This parameter is ignored if the first parameter is an object)
 * 
 * Use:  OE mobile developer instantiates a session for each combination of service URL
 *       and user that's required. Could do them all first, or could do them as 
 *       they're needed. 
 *       Developer instantiates JDSOs as needed, providing session instance for each
 *       Usually all of the JSDOs will use the same session, but if a client-side
 *       service needs resources from more than one REST app, there would need to be more
 *       than one session 
 * 
 */
progress.data.Session = function Session( catalogURL, serviceName ) {

	/* constants - define them as properties via the defineProperty()
	 * function, which has "writable" and "configurable" parameters that both 
	 * default to false, so these calls create properties that are read-only
	 */
	if ((typeof Object.defineProperty) == 'function') {
	    Object.defineProperty( progress.data.Session, 'LOGIN_SUCCESS', { 
	    		                value: 1, enumerable: true }   );
	    Object.defineProperty( progress.data.Session, 'LOGIN_AUTHENTICATION_FAILURE', {
	        					value: 2, enumerable: true }  );
	    Object.defineProperty( progress.data.Session, 'LOGIN_GENERAL_FAILURE', {
	    						value:  3, enumerable: true });
	}
	else {
		progress.data.Session.LOGIN_SUCCESS = 1;
		progress.data.Session.LOGIN_AUTHENTICATION_FAILURE = 2;
		progress.data.Session.LOGIN_GENERAL_FAILURE = 3;
	}
	
	// Public read-write properties 
    var _userName = null;
    this.getUserName= function(){
    	return _userName;
    };
    this.setUserName= function( uname ){
    	if ( _loginResult === progress.data.Session.LOGIN_SUCCESS) {
        	throw new Error( "Cannot change Session user name -- already logged in" );
    	}
    	_userName = uname;
    };

    var _password = null;
    this.getPassword= function(){
    	return _password;
    };
    this.setPassword = function( pw ){
    	if ( _loginResult === progress.data.Session.LOGIN_SUCCESS) {
        	throw new Error( "Cannot change Session password -- already logged in" );
    	}
    	_password = pw;
    };

    // Public read-only "properties"
    var _catalogURL = null;
    this.getCatalogURL = function(){
    	return _catalogURL;
    };

    var _serviceName = null;
    this.getServiceName= function(){
    	return _serviceName;
    };
    
    var _loginResult = -1;
    this.getLoginResult = function(){
    	return _loginResult;
    };
    
    var _loginHttpStatus = -1;
    this.getLoginHttpStatus= function(){
    	return _loginHttpStatus;
    };

    // Initialize Session using init values
    if ( arguments.length == 2 ) {
    	_catalogURL = catalogURL;
    	_serviceName = serviceName;
    }
    else if ( arguments.length == 1 ) {
    	_catalogURL = catalogURL;  // inelegant because we have this above, but straightforward
    }
    else {
    	throw new Error( 'Session constructor arguments must be catalog url and, optionally, service name');
    }
    
    
    if ( typeof _catalogURL != "string" ) {
    	throw new Error( "Unable to create Session object - missing catalog URL or service name" );
    }
   
    // Functions
	this._openRequest = function (xhr, verb, url, async) {
		if (xhr.jsdo && xhr.jsdo._session && xhr.jsdo._session._jsessionid) {
			// TODO: ** Test protocol, host and port in addition to path to ensure that jsessionid is only sent
			// when request applies to the location

			if (url.substring(0, xhr.jsdo._session._jsessionPath.length) == xhr.jsdo._session._jsessionPath) {
				// jessionid when read from a URL would already be encoded - call to encodeURIComponent() is not required
				var jsessionidStr = ";jsessionid=" + xhr.jsdo._session._jsessionid;
				var index = url.indexOf('?');
				if (index == -1) {
					url += jsessionidStr;
				}
				else {
					url = url.substring(0, index) + jsessionidStr + url.substring(index);
				}
			}
		}
		xhr.open(verb, url, async, _userName, _password);
		// do we need to set the withCredentials property to true, for CORS ?
	};

	/* login
	 *    
	 */
	this.login = function ( userName, password ) {
		
		if ( arguments.length > 0) {
		    if ( arguments[0] ) {
		    	_userName = arguments[0];
			}
			
			if ( arguments[1]) {
				_password = arguments[1];
			}
		}
		
		/* should we check whether catalog already loaded? */
		var catalog = new progress.data.JSDO( { _session: this, url: _catalogURL,
			                     _async: false, autoSaveChanges: false, autoFill: true } );
		_loginHttpStatus = catalog.lastHttpStatus;
		
        if ((_loginHttpStatus == 200) || (_loginHttpStatus == 0)) {
			progress.data.JSDOManager.addCatalog( catalog );
			var services = catalog.getData()[0].services;
			if (!services) {
				throw new Error( 'Cannot find services property in catalog file' );
			}
			_serviceName = services[0].name;
		    _loginResult = progress.data.Session.LOGIN_SUCCESS;
			progress.data.JSDOManager.addSession(_serviceName, this);
	    }
        else if (_loginHttpStatus == 401) {
		    _loginResult = progress.data.Session.LOGIN_AUTHENTICATION_FAILURE;
		    /*		
		    add logic to handle authentication failures -- 
				determine where to get the username and password		
				xhr.open('GET', this._URL, async, this.username, this.password);		
			if ((typeof xhr.onAuthenticationErrorFn) == 'function')
				xhr.onAuthenticationErrorFn(this);
    		 */
	    }
	    else {
		    _loginResult = progress.data.Session.LOGIN_GENERAL_FAILURE;		    
	    }
		return _loginResult;
	};

}; // End of Session
})();
