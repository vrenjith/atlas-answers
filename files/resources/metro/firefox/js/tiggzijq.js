(function (ns) {
    "use strict";
    ns.persistentStorage = localStorage;
}(this.$t = this.$t || {}));

/* Simple JavaScript Inheritance
 * By John Resig http://ejohn.org/
 * MIT Licensed.
 */
// Inspired by base2 and Prototype
(function(ns){
  "use strict";
  var initializing = false, fnTest = /xyz/.test(function(){xyz;}) ? /\b_super\b/ : /.*/;

  // The base Class implementation (does nothing)
  ns.Class = function(){};
 
  // Create a new Class that inherits from this class
  ns.Class.extend = function __self(prop) {
    var _super = this.prototype;
   
    // Instantiate a base class (but only create the instance,
    // don't run the init constructor)
    initializing = true;
    var prototype = new this();
    initializing = false;
   
    // Copy the properties over onto the new prototype
    for (var name in prop) {
      // Check if we're overwriting an existing function
      prototype[name] = typeof prop[name] == "function" &&
        typeof _super[name] == "function" && fnTest.test(prop[name]) ?
        (function(name, fn){
          return function() {
            var tmp = this._super;
           
            // Add a new ._super() method that is the same method
            // but on the super-class
            this._super = _super[name];
           
            // The method only need to be bound temporarily, so we
            // remove it when we're done executing
            var ret = fn.apply(this, arguments);       
            this._super = tmp;
           
            return ret;
          };
        })(name, prop[name]) :
        prop[name];
    }
   
    // The dummy class constructor
    function Class() {
      // All construction is actually done in the init method
      if ( !initializing && this.init )
        this.init.apply(this, arguments);
    }
   
    // Populate our constructed prototype object
    Class.prototype = prototype;
   
    // Enforce the constructor to be what we expect
    Class.prototype.constructor = Class;

    // And make this class extendable
    //Class.extend = arguments.callee;
    Class.extend = __self;
   
    return Class;
  };
}(this.$t = this.$t || {}));

(function (ns) {
    "use strict";
}(this.$t.util = this.$t.util || 
{
	select:function(query){
       	    return document.querySelector(query);
	},
	selectAll:function(query){
           return document.querySelectorAll(query);
	},
	isFunction:$.isFunction,
	isArray:$.isArray,
	isWindow:$.isWindow,
	isNumeric:$.isNumeric,
	type:$.type,
	isPlainObject:$.isPlainObject,
	isEmptyObject:$.isEmptyObject,
	each:$.each,
	trim:$.trim,
	extend:$.extend,
	merge:$.merge,
	proxy:$.proxy,
	param:$.param,	
	parseJSON:$.parseJSON,
	parseXML:$.parseXML,
	xml2json:$.xml2json
}));

if (this.$t.util)
    (function ($) {
	"use strict";
        var _this = $;
        // Add function to Util namespace
        $.extend({
            dataToXml:function (data) {
                return "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<inputParameters>" + _this.objectToXml(data) + "</inputParameters>";
            },

            objectToXml:function (data) {
                var encodedData = "",
                    key;
                for (key in data) {
                    if (data.hasOwnProperty(key)) {
                        var value = data[key];
                        if (typeof value !== 'string') {
                            value = _this.objectToXml(value);
                        }
                        encodedData += "<" + key + ">" + value + "</" + key + ">";
                    }
                }
                return encodedData;
            },

            getAttribute:function (element, attrName) {
                if (attrName === "@") {
                    return element.innerHTML;
                } else {
                    return element[attrName] || element.getAttribute(attrName);
                }
            },

            setAttribute:function (element, attrName, value) {
                if (attrName === "@") {
                    element.innerHTML = value;
                } else {
                    element.setAttribute(attrName, value);
                }
            },

            cloneObject:function cloneObject(src) {
                var newObj = (src instanceof Array) ? [] : {};
                for (var i in src) {
                    if (src[i] && typeof src[i] == "object") {
                        newObj[i] = cloneObject(src[i]);
                    } else {
                        newObj[i] = src[i];
                    }
                }
                return newObj;
            },

            simplifyPath:function (path) {
                var resultPath = new Array(),
                    resultIndex = 0;
                for (var i = 0; i < path.length; i++) {
                    var pathPart = path[i],
                        openBracketIndex = pathPart.lastIndexOf("["),
                        closeBracketIndex = pathPart.lastIndexOf("]");
                    if (openBracketIndex != -1 && closeBracketIndex != -1) {
                        resultPath[resultIndex] = pathPart.substring(0, openBracketIndex);
                        ++resultIndex;
                        resultPath[resultIndex] = pathPart.substring(openBracketIndex + 1, closeBracketIndex);
                        ++resultIndex;
                    } else {
                        resultPath[resultIndex] = pathPart;
                        ++resultIndex;
                    }
                }

                return resultPath;
            },

            setDataValueByPath:function (container, path, value) {
                var destinationObject = $.getDestinationObject($.simplifyPath(path), container);
                destinationObject[path[path.length - 1]] = value;
            },

            getDestinationObject:function getDestinationObject(path, container) {
                if (path.length == 1) {
                    return container;
                } else {
                    var pathPart = path[0];
                    path.shift();
                    container[pathPart] = {};
                    return getDestinationObject(path, container[pathPart]);
                }
            }
        });

    }(this.$t.util));
/*
 ### jQuery XML to JSON Plugin v1.1 - 2008-07-01 ###
 * http://www.fyneworks.com/ - diego@fyneworks.com
 * Dual licensed under the MIT and GPL licenses:
 *   http://www.opensource.org/licenses/mit-license.php
 *   http://www.gnu.org/licenses/gpl.html
 ###
 Website: http://www.fyneworks.com/jquery/xml-to-json/
 *//*
 # INSPIRED BY: http://www.terracoder.com/
 AND: http://www.thomasfrank.se/xml_to_json.html
 AND: http://www.kawa.net/works/js/xml/objtree-e.html
 *//*
 This simple script converts XML (document of code) into a JSON object. It is the combination of 2
 'xml to json' great parsers (see below) which allows for both 'simple' and 'extended' parsing modes.
 */
// Avoid collisions

;
if (this.$t.util) (function ($) {
   "use strict";
   var _this = $;

    // Add function to Util namespace
    $.extend({

        // converts xml documents and xml text to json object
        xml2json:function (xml, extended) {
            if (!xml) return {}; // quick fail

            //### PARSER LIBRARY
            // Core function
            function parseXML(node, simple) {
                if (!node) return null;
                var txt = '', obj = null, att = null;
                var nt = node.nodeType, nn = jsVar(node.localName || node.nodeName);
                var nv = node.text || node.nodeValue || '';
                /*DBG*/ //if(window.console) console.log(['x2j',nn,nt,nv.length+' bytes']);
                if (node.childNodes) {
                    if (node.childNodes.length > 0) {
                        /*DBG*/ //if(window.console) console.log(['x2j',nn,'CHILDREN',node.childNodes]);
                        _this.each(node.childNodes, function (n, cn) {
                            var cnt = cn.nodeType, cnn = jsVar(cn.localName || cn.nodeName);
                            var cnv = cn.text || cn.nodeValue || '';
                            /*DBG*/ //if(window.console) console.log(['x2j',nn,'node>a',cnn,cnt,cnv]);
                            if (cnt == 8) {
                                /*DBG*/ //if(window.console) console.log(['x2j',nn,'node>b',cnn,'COMMENT (ignore)']);
                                return; // ignore comment node
                            }
                            else if (cnt == 3 || cnt == 4 || !cnn) {
                                // ignore white-space in between tags
                                if (cnv.match(/^\s+$/)) {
                                    /*DBG*/ //if(window.console) console.log(['x2j',nn,'node>c',cnn,'WHITE-SPACE (ignore)']);
                                    return;
                                }
                                ;
                                /*DBG*/ //if(window.console) console.log(['x2j',nn,'node>d',cnn,'TEXT']);
                                txt += cnv.replace(/^\s+/, '').replace(/\s+$/, '');
                                // make sure we ditch trailing spaces from markup
                            }
                            else {
                                /*DBG*/ //if(window.console) console.log(['x2j',nn,'node>e',cnn,'OBJECT']);
                                obj = obj || {};
                                if (obj[cnn]) {
                                    /*DBG*/ //if(window.console) console.log(['x2j',nn,'node>f',cnn,'ARRAY']);

                                    // http://forum.jquery.com/topic/jquery-jquery-xml2json-problems-when-siblings-of-the-same-tagname-only-have-a-textnode-as-a-child
                                    if (!obj[cnn].length) obj[cnn] = myArr(obj[cnn]);
                                    obj[cnn] = myArr(obj[cnn]);

                                    obj[cnn][ obj[cnn].length ] = parseXML(cn, true/* simple */);
                                    obj[cnn].length = obj[cnn].length;
                                }
                                else {
                                    /*DBG*/ //if(window.console) console.log(['x2j',nn,'node>g',cnn,'dig deeper...']);
                                    obj[cnn] = parseXML(cn);
                                }
                                ;
                            }
                            ;
                        });
                    }
                    ;//node.childNodes.length>0
                }
                ;//node.childNodes
                if (node.attributes) {
                    if (node.attributes.length > 0) {
                        /*DBG*/ //if(window.console) console.log(['x2j',nn,'ATTRIBUTES',node.attributes])
                        att = {};
                        obj = obj || {};
                        _this.each(node.attributes, function (a, at) {
                            var atn = jsVar(at.name), atv = at.value;
                            att[atn] = atv;
                            if (obj[atn]) {
                                /*DBG*/ //if(window.console) console.log(['x2j',nn,'attr>',atn,'ARRAY']);

                                // http://forum.jquery.com/topic/jquery-jquery-xml2json-problems-when-siblings-of-the-same-tagname-only-have-a-textnode-as-a-child
                                //if(!obj[atn].length) obj[atn] = myArr(obj[atn]);//[ obj[ atn ] ];
                                obj[cnn] = myArr(obj[cnn]);

                                obj[atn][ obj[atn].length ] = atv;
                                obj[atn].length = obj[atn].length;
                            }
                            else {
                                /*DBG*/ //if(window.console) console.log(['x2j',nn,'attr>',atn,'TEXT']);
                                obj[atn] = atv;
                            }
                            ;
                        });
                        //obj['attributes'] = att;
                    }
                    ;//node.attributes.length>0
                }
                ;//node.attributes
                if (obj) {
                    obj = _this.extend((txt != '' ? new String(txt) : {}), /* {text:txt},*/ obj || {}/*, att || {}*/);
                    txt = (obj.text) ? (typeof(obj.text) == 'object' ? obj.text : [obj.text || '']).concat([txt]) : txt;
                    if (txt) obj.text = txt;
                    txt = '';
                }
                ;
                var out = obj || txt;
                //console.log([extended, simple, out]);
                if (extended) {
                    if (txt) out = {};//new String(out);
                    txt = out.text || txt || '';
                    if (txt) out.text = txt;
                    if (!simple) out = myArr(out);
                }
                ;
                return out;
            }

            ;// parseXML
            // Core Function End
            // Utility functions
            var jsVar = function (s) {
                return String(s || '').replace(/-/g, "_");
            };

            // NEW isNum function: 01/09/2010
            // Thanks to Emile Grau, GigaTecnologies S.L., www.gigatransfer.com, www.mygigamail.com
            function isNum(s) {
                // based on utility function isNum from xml2json plugin (http://www.fyneworks.com/ - diego@fyneworks.com)
                // few bugs corrected from original function :
                // - syntax error : regexp.test(string) instead of string.test(reg)
                // - regexp modified to accept  comma as decimal mark (latin syntax : 25,24 )
                // - regexp modified to reject if no number before decimal mark  : ".7" is not accepted
                // - string is "trimmed", allowing to accept space at the beginning and end of string
                var regexp = /^((-)?([0-9]+)(([\.\,]{0,1})([0-9]+))?$)/
                return (typeof s == "number") || regexp.test(String((s && typeof s == "string") ? _this.trim(s) : ''));
            }

            ;
            // OLD isNum function: (for reference only)
            //var isNum = function(s){ return (typeof s == "number") || String((s && typeof s == "string") ? s : '').test(/^((-)?([0-9]*)((\.{0,1})([0-9]+))?$)/); };

            var myArr = function (o) {

                // http://forum.jquery.com/topic/jquery-jquery-xml2json-problems-when-siblings-of-the-same-tagname-only-have-a-textnode-as-a-child
                //if(!o.length) o = [ o ]; o.length=o.length;
                if (!_this.isArray(o)) o = [ o ];
                o.length = o.length;

                // here is where you can attach additional functionality, such as searching and sorting...
                return o;
            };
            // Utility functions End
            //### PARSER LIBRARY END

            // Convert plain text to xml
            if (typeof xml == 'string') xml = _this.text2xml(xml);

            // Quick fail if not xml (or if this is a node)
            if (!xml.nodeType) return;
            if (xml.nodeType == 3 || xml.nodeType == 4) return xml.nodeValue;

            // Find xml root node
            var root = (xml.nodeType == 9) ? xml.documentElement : xml;

            // Convert xml to json
            var out = parseXML(root, true /* simple */);

            // Clean-up memory
            xml = null;
            root = null;

            // Send output
            return out;
        },

        // Convert text to XML DOM
        text2xml:function (str) {
            // NOTE: I'd like to use jQuery for this, but jQuery makes all tags uppercase
            //return $(xml)[0];
            var out;
            try {
                var xml = (_this.browser.msie) ? new ActiveXObject("Microsoft.XMLDOM") : new DOMParser();
                xml.async = false;
            } catch (e) {
                throw new Error("XML Parser could not be instantiated")
            }
            ;
            try {
                if (_this.browser.msie) out = (xml.loadXML(str)) ? xml : false;
                else out = xml.parseFromString(str, "text/xml");
            } catch (e) {
                throw new Error("Error parsing XML string")
            }
            ;
            return out;
        }

    }); // extend $

}(this.$t.util));
/*
Copyright (c) 1998 - 2009, Paul Johnston & Contributors
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, 
are permitted provided that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list
of conditions and the following disclaimer. Redistributions in binary form must 
reproduce the above copyright notice, this list of conditions and the following 
disclaimer in the documentation and/or other materials provided with the 
distribution.

Neither the name of the author nor the names of its contributors may be used to
endorse or promote products derived from this software without specific prior
written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
if (this.$t.util)
(function (ns) {
    "use strict";
    /*
     * Configurable variables. You may need to tweak these to be compatible with
     * the server-side, but the defaults work in most cases.
     */
    ns.hexcase = 0;  /* hex output format. 0 - lowercase; 1 - uppercase        */
    ns.b64pad = ""; /* base-64 pad character. "=" for strict RFC compliance   */

    /*
     * These are the functions you'll usually want to call
     * They take string arguments and return either hex or base-64 encoded strings
     */
    ns.hex_sha1 = function (s) { return rstr2hex(rstr_sha1(str2rstr_utf8(s))); }
    ns.b64_sha1 = function (s) { return rstr2b64(rstr_sha1(str2rstr_utf8(s))); }
    ns.any_sha1 = function (s, e) { return rstr2any(rstr_sha1(str2rstr_utf8(s)), e); }
    ns.hex_hmac_sha1 = function (k, d)
    { return rstr2hex(rstr_hmac_sha1(str2rstr_utf8(k), str2rstr_utf8(d))); }
    ns.b64_hmac_sha1 = function (k, d)
    { return rstr2b64(rstr_hmac_sha1(str2rstr_utf8(k), str2rstr_utf8(d))); }
    ns.any_hmac_sha1 = function (k, d, e)
    { return rstr2any(rstr_hmac_sha1(str2rstr_utf8(k), str2rstr_utf8(d)), e); }

    /*
     * Perform a simple self-test to see if the VM is working
     */
    function sha1_vm_test() {
        return hex_sha1("abc").toLowerCase() == "a9993e364706816aba3e25717850c26c9cd0d89d";
    }

    /*
     * Calculate the SHA1 of a raw string
     */
    function rstr_sha1(s) {
        return binb2rstr(binb_sha1(rstr2binb(s), s.length * 8));
    }

    /*
     * Calculate the HMAC-SHA1 of a key and some data (raw strings)
     */
    function rstr_hmac_sha1(key, data) {
        var bkey = rstr2binb(key);
        if (bkey.length > 16) bkey = binb_sha1(bkey, key.length * 8);

        var ipad = Array(16), opad = Array(16);
        for (var i = 0; i < 16; i++) {
            ipad[i] = bkey[i] ^ 0x36363636;
            opad[i] = bkey[i] ^ 0x5C5C5C5C;
        }

        var hash = binb_sha1(ipad.concat(rstr2binb(data)), 512 + data.length * 8);
        return binb2rstr(binb_sha1(opad.concat(hash), 512 + 160));
    }

    /*
     * Convert a raw string to a hex string
     */
    function rstr2hex(input) {
        try { ns.hexcase } catch (e) { ns.hexcase = 0; }
        var hex_tab = ns.hexcase ? "0123456789ABCDEF" : "0123456789abcdef";
        var output = "";
        var x;
        for (var i = 0; i < input.length; i++) {
            x = input.charCodeAt(i);
            output += hex_tab.charAt((x >>> 4) & 0x0F)
                   + hex_tab.charAt(x & 0x0F);
        }
        return output;
    }

    /*
     * Convert a raw string to a base-64 string
     */
    function rstr2b64(input) {
        try { ns.b64pad } catch (e) { ns.b64pad = ''; }
        var tab = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
        var output = "";
        var len = input.length;
        for (var i = 0; i < len; i += 3) {
            var triplet = (input.charCodeAt(i) << 16)
                        | (i + 1 < len ? input.charCodeAt(i + 1) << 8 : 0)
                        | (i + 2 < len ? input.charCodeAt(i + 2) : 0);
            for (var j = 0; j < 4; j++) {
                if (i * 8 + j * 6 > input.length * 8) output += ns.b64pad;
                else output += tab.charAt((triplet >>> 6 * (3 - j)) & 0x3F);
            }
        }
        return output;
    }

    /*
     * Convert a raw string to an arbitrary string encoding
     */
    function rstr2any(input, encoding) {
        var divisor = encoding.length;
        var remainders = Array();
        var i, q, x, quotient;

        /* Convert to an array of 16-bit big-endian values, forming the dividend */
        var dividend = Array(Math.ceil(input.length / 2));
        for (i = 0; i < dividend.length; i++) {
            dividend[i] = (input.charCodeAt(i * 2) << 8) | input.charCodeAt(i * 2 + 1);
        }

        /*
         * Repeatedly perform a long division. The binary array forms the dividend,
         * the length of the encoding is the divisor. Once computed, the quotient
         * forms the dividend for the next step. We stop when the dividend is zero.
         * All remainders are stored for later use.
         */
        while (dividend.length > 0) {
            quotient = Array();
            x = 0;
            for (i = 0; i < dividend.length; i++) {
                x = (x << 16) + dividend[i];
                q = Math.floor(x / divisor);
                x -= q * divisor;
                if (quotient.length > 0 || q > 0)
                    quotient[quotient.length] = q;
            }
            remainders[remainders.length] = x;
            dividend = quotient;
        }

        /* Convert the remainders to the output string */
        var output = "";
        for (i = remainders.length - 1; i >= 0; i--)
            output += encoding.charAt(remainders[i]);

        /* Append leading zero equivalents */
        var full_length = Math.ceil(input.length * 8 /
                                          (Math.log(encoding.length) / Math.log(2)))
        for (i = output.length; i < full_length; i++)
            output = encoding[0] + output;

        return output;
    }

    /*
     * Encode a string as utf-8.
     * For efficiency, this assumes the input is valid utf-16.
     */
    function str2rstr_utf8(input) {
        var output = "";
        var i = -1;
        var x, y;

        while (++i < input.length) {
            /* Decode utf-16 surrogate pairs */
            x = input.charCodeAt(i);
            y = i + 1 < input.length ? input.charCodeAt(i + 1) : 0;
            if (0xD800 <= x && x <= 0xDBFF && 0xDC00 <= y && y <= 0xDFFF) {
                x = 0x10000 + ((x & 0x03FF) << 10) + (y & 0x03FF);
                i++;
            }

            /* Encode output as utf-8 */
            if (x <= 0x7F)
                output += String.fromCharCode(x);
            else if (x <= 0x7FF)
                output += String.fromCharCode(0xC0 | ((x >>> 6) & 0x1F),
                                              0x80 | (x & 0x3F));
            else if (x <= 0xFFFF)
                output += String.fromCharCode(0xE0 | ((x >>> 12) & 0x0F),
                                              0x80 | ((x >>> 6) & 0x3F),
                                              0x80 | (x & 0x3F));
            else if (x <= 0x1FFFFF)
                output += String.fromCharCode(0xF0 | ((x >>> 18) & 0x07),
                                              0x80 | ((x >>> 12) & 0x3F),
                                              0x80 | ((x >>> 6) & 0x3F),
                                              0x80 | (x & 0x3F));
        }
        return output;
    }

    /*
     * Encode a string as utf-16
     */
    function str2rstr_utf16le(input) {
        var output = "";
        for (var i = 0; i < input.length; i++)
            output += String.fromCharCode(input.charCodeAt(i) & 0xFF,
                                          (input.charCodeAt(i) >>> 8) & 0xFF);
        return output;
    }

    function str2rstr_utf16be(input) {
        var output = "";
        for (var i = 0; i < input.length; i++)
            output += String.fromCharCode((input.charCodeAt(i) >>> 8) & 0xFF,
                                           input.charCodeAt(i) & 0xFF);
        return output;
    }

    /*
     * Convert a raw string to an array of big-endian words
     * Characters >255 have their high-byte silently ignored.
     */
    function rstr2binb(input) {
        var output = Array(input.length >> 2);
        for (var i = 0; i < output.length; i++)
            output[i] = 0;
        for (var i = 0; i < input.length * 8; i += 8)
            output[i >> 5] |= (input.charCodeAt(i / 8) & 0xFF) << (24 - i % 32);
        return output;
    }

    /*
     * Convert an array of big-endian words to a string
     */
    function binb2rstr(input) {
        var output = "";
        for (var i = 0; i < input.length * 32; i += 8)
            output += String.fromCharCode((input[i >> 5] >>> (24 - i % 32)) & 0xFF);
        return output;
    }

    /*
     * Calculate the SHA-1 of an array of big-endian words, and a bit length
     */
    function binb_sha1(x, len) {
        /* append padding */
        x[len >> 5] |= 0x80 << (24 - len % 32);
        x[((len + 64 >> 9) << 4) + 15] = len;

        var w = Array(80);
        var a = 1732584193;
        var b = -271733879;
        var c = -1732584194;
        var d = 271733878;
        var e = -1009589776;

        for (var i = 0; i < x.length; i += 16) {
            var olda = a;
            var oldb = b;
            var oldc = c;
            var oldd = d;
            var olde = e;

            for (var j = 0; j < 80; j++) {
                if (j < 16) w[j] = x[i + j];
                else w[j] = bit_rol(w[j - 3] ^ w[j - 8] ^ w[j - 14] ^ w[j - 16], 1);
                var t = safe_add(safe_add(bit_rol(a, 5), sha1_ft(j, b, c, d)),
                                 safe_add(safe_add(e, w[j]), sha1_kt(j)));
                e = d;
                d = c;
                c = bit_rol(b, 30);
                b = a;
                a = t;
            }

            a = safe_add(a, olda);
            b = safe_add(b, oldb);
            c = safe_add(c, oldc);
            d = safe_add(d, oldd);
            e = safe_add(e, olde);
        }
        return Array(a, b, c, d, e);

    }

    /*
     * Perform the appropriate triplet combination function for the current
     * iteration
     */
    function sha1_ft(t, b, c, d) {
        if (t < 20) return (b & c) | ((~b) & d);
        if (t < 40) return b ^ c ^ d;
        if (t < 60) return (b & c) | (b & d) | (c & d);
        return b ^ c ^ d;
    }

    /*
     * Determine the appropriate additive constant for the current iteration
     */
    function sha1_kt(t) {
        return (t < 20) ? 1518500249 : (t < 40) ? 1859775393 :
               (t < 60) ? -1894007588 : -899497514;
    }

    /*
     * Add integers, wrapping at 2^32. This uses 16-bit operations internally
     * to work around bugs in some JS interpreters.
     */
    function safe_add(x, y) {
        var lsw = (x & 0xFFFF) + (y & 0xFFFF);
        var msw = (x >> 16) + (y >> 16) + (lsw >> 16);
        return (msw << 16) | (lsw & 0xFFFF);
    }

    /*
     * Bitwise rotate a 32-bit number to the left.
     */
    function bit_rol(num, cnt) {
        return (num << cnt) | (num >>> (32 - cnt));
    }
}(this.$t.util));
(function (ns) {
    "use strict";

    var mappingRendererTypes = [];

    ns.addMappingRendererType = function (mappingRendererType) {
        mappingRendererTypes.push(mappingRendererType);
    };

    ns.render = function (mappings, data) {
        for (var i = 0, mappingsCount = mappings.length; i < mappingsCount; i++) {
            var mapping = mappings[i],
                renderer = undefined;
            for (var rendererIndex = 0; rendererIndex < mappingRendererTypes.length; rendererIndex++) {
                var rendererType = mappingRendererTypes[rendererIndex];
                if (rendererType.prototype.isSuitable(mapping)) {
                    renderer = new rendererType(ns.util.cloneObject(mapping), data);
                    break;
                }
            }

            if (!renderer) {
                renderer = new ns.DefaultMappingRenderer(ns.util.cloneObject(mapping), data);
            }

            renderer.render();
        }
    };

    ns.DefaultMappingRenderer = function (mapping, data) {
        this.mapping = mapping;
        this.data = data;
    };

    ns.DefaultMappingRenderer.prototype.isSuitable = function (mapping) {
        return true;
    };

    ns.DefaultMappingRenderer.prototype.getValueFromPath = function getValueFromPath(path, data) {
        if (path.length == 1) {
            return data[path[0]];
        } else {
            var containerObject = data[path[0]];
            if (containerObject) {
                path.shift();
                return getValueFromPath(path, containerObject);
            } else {
                return undefined;
            }
        }
    };

    ns.DefaultMappingRenderer.prototype.cloneTemplate = function (src) {
        if (src) {
            var node = src.cloneNode(true);
            node.style.display = "block";
            return node;
        }
    };

    ns.DefaultMappingRenderer.prototype.getAllChildren = function getAllChildren(node,result) {
        if(node) {
            var childNodes = node.childNodes;

            if (childNodes) {
                for (var nodeIndex = 0; nodeIndex < childNodes.length; nodeIndex++) {
                    result = result.concat(childNodes[nodeIndex]);
                }
                for (var i = 0; i < childNodes.length; i++) {
                    result = getAllChildren(childNodes[i], result);
                }
            }
            return result;
        } else {
            return result;
        }
    };

    ns.DefaultMappingRenderer.prototype.handleMappingRuleForClonedTemplate = function (rule, template, dataItem) {
        if (rule && template && dataItem) {
            var clonedChildNodes = this.getAllChildren(template, []);
            for(var clonedChildIndex=0; clonedChildIndex < clonedChildNodes.length; clonedChildIndex++) {
                var clonedChild = clonedChildNodes[clonedChildIndex];
                if(clonedChild.getAttribute && clonedChild.getAttribute("dsid")===rule.ID) {
                    var valueFromPath = this.getValueFromPath(rule.PATH,dataItem);
                    if(rule.TRANSFORMATION) {
                        valueFromPath = rule.TRANSFORMATION(valueFromPath,clonedChild);
                    }
                    ns.util.setAttribute(clonedChild, rule.ATTR, valueFromPath);
                }
            }
        }
    };

    ns.DefaultMappingRenderer.prototype.clearContainer = function (container, template) {
        if (container && template) {
            container.innerHTML = "";
            container.appendChild(template);

        }
    };

    ns.DefaultMappingRenderer.prototype.render = function () {
        var isForLocalStorage = this.mapping.ID === "___local_storage___",
            transformationFunction = this.mapping.TRANSFORMATION,
            attribute = this.mapping.ATTR,
            value = this.getValueFromPath(ns.util.simplifyPath(this.mapping.PATH), this.data),
            element = isForLocalStorage ? undefined : ns.util.select("#" + this.mapping.ID);

        if (this.mapping.SET) {
            var template = ns.util.select("#" + this.mapping.ID + " [ds-role='template']");
            if (element && template && element.childNodes) {
                this.clearContainer(element,template);

                for (var key in value) {
                    if (value.hasOwnProperty(key)) {
                        var currentDataItem = value[key];
                        var clonedTemplate = this.cloneTemplate(template);

                        for (var mappingKey in this.mapping.SET) {
                            if (this.mapping.SET.hasOwnProperty(mappingKey)) {
                                this.handleMappingRuleForClonedTemplate(this.mapping.SET[mappingKey], clonedTemplate, currentDataItem);
                            }
                        }
                        element.appendChild(clonedTemplate);
                    }
                }
            }
        } else {
            if (transformationFunction) {
                value = transformationFunction(value, element);
            }

            if (isForLocalStorage) {
                ns.persistentStorage.setItem(attribute, value);
            } else {
                ns.util.setAttribute(element, attribute, value);
            }
        }

    };

}(this.$t = this.$t || {}));
/**
 * Request data builder
 */
(function (ns) {
    "use strict";
    var mappingProcessors = [];

    ns.addMappingProcessor = function (processor) {
        mappingProcessors.push(processor);
    };

    ns.buildRequestConfiguration = function (mappings) {
        var resultHeaders = {},
            resultParameters = {};

        for (var i = 0, mappingsCount = mappings.length; i < mappingsCount; i++) {
            var parameters = undefined,
                headers = undefined,
                processor = undefined;

                var mapping = mappings[i];

                for (var k = 0; k < mappingProcessors.length; k++) {
                    var processorType = mappingProcessors[k];
                    if (processorType.prototype.isSuitable(mapping)) {
                        processor = new processorType(mapping);
                    }
                }

                //Suitable processor type not found so we will use the default one
                if (processor == undefined) {
                    processor = new ns.DefaultMappingProcessor(mapping);
                }

                processor.process();
                parameters = processor.getParameters();
                headers = processor.getHeaders();
                resultParameters = ns.util.extend(resultParameters, parameters);
                resultHeaders = ns.util.extend(resultHeaders, headers);

        }
        return {parameters : resultParameters, headers : resultHeaders};
    };

    /**
     * Default mapping processor. In case you need extra functionality for some mapping or element all you need is to
     * implement class like this or extend this class
     *
     * @param mapping object that represents one out-mapping
     * @constructor
     */
    ns.DefaultMappingProcessor = function (mapping) {
        var parameters = {};
        var headers = {};

        this.getMapping = function () {
            return mapping;
        };

        this.getHeaders = function () {
            return headers;
        };

        this.getParameters = function () {
            return parameters;
        }
    };

    /**
     * Checks the suitability of mapping processor and given mapping
     *
     * @param mapping
     * @return {Boolean} true if processor is suitable
     */
    ns.DefaultMappingProcessor.prototype.isSuitable = function (mapping) {
        return true;
    };

    ns.DefaultMappingProcessor.prototype.process = function () {
        var mapping = this.getMapping(),
            headers = this.getHeaders(),
            parameters = this.getParameters();

        if (mapping.SET) {

        } else {
            var isHeader = !!mapping.HEADER,
                predefinedValue = !mapping.ID,
                isFromLocalStorage = mapping.ID === "___local_storage___",
                transformationFunction = mapping.TRANSFORMATION,
                attribute = mapping.ATTR,
                value;

            if (predefinedValue) {
                value = attribute;
            } else {
                if (isFromLocalStorage) {
                    value = ns.persistentStorage.getItem(attribute);
                } else {
                    value = ns.util.getAttribute(ns.util.select("#" + mapping.ID), attribute);
                }
            }

            if (transformationFunction) {
                value = transformationFunction(value);
            }
            if(value) {
                ns.util.setDataValueByPath(isHeader ? headers : parameters, mapping["PATH"], value);
            }
        }
    }

}(this.$t = this.$t || {}));
/**
 * DataSource
 */
(function (ns) {
    "use strict";
    ns.DataSource = function (service, options) {
        this.service = service;
        this.options = options;
        this.responseMapping = options.responseMapping || [];
        this.requestMapping = options.requestMapping || [];
        this.requestOptions = ns.util.extend({}, this.requestDefaults, options);
    };

    var proto = ns.DataSource.prototype;

    proto.execute = function (settings) {
        this.service.process(this.buildRequestSettings(settings));
    };

    proto.updateComponents = function (data) {
        ns.render(this.responseMapping, data);
    };

    proto.onBeforeSend = function () {

    };

    proto.onSuccess = function (data) {
        if (this.service.requestOptions.dataType === "xml") {
            this.updateComponents(ns.util.xml2json(data));
        } else {
            this.updateComponents(data);
        }

        if (this.options.onSuccess) {
            this.options.onSuccess.apply(this, ns.util.merge([], arguments));
        }
    };

    proto.onComplete = function () {
        if (this.options.onComplete) {
            this.options.onComplete.apply(this, ns.util.merge([], arguments));
        }
    };

    proto.onError = function () {

        if (this.options.onError) {
            this.options.onError.apply(this, ns.util.merge([], arguments));
        }
    };

    proto.handleData = function () {
        var args = $t.util.merge([], arguments);
        var data = args[0];

        if (ns.util.type(data) === 'string') {
            data = $t.util.parseJSON(data);
            args[0] = data;
        }

        this.onSuccess.apply(this, args);
    };

    proto.buildRequestSettings = function (settings) {

	if (settings.SC != undefined && settings.SC.version != undefined) {
            var auth = settings.SC;
            switch (auth.version) {
                case "1.0a": settings.Authorization = auth.header((settings.url !== undefined) ? settings.url : this.service.requestOptions.url);
                    break;
                case "2.0": settings.Authorization = auth.header;
                    break;
            }
            delete settings.SC;
        }
        else if (settings.SC === null) {
            delete settings.SC;
        }

        var proxy = ns.util.proxy,
            handlers = {
                'success': proxy(this.handleData, this),
                'error': proxy(this.onError, this),
                'complete': proxy(this.onComplete, this),
                'beforeSend': proxy(this.onBeforeSend, this)
            };
        settings = ns.util.extend(handlers, this.service.requestOptions, settings || {});

        var requestConfiguration = ns.buildRequestConfiguration(this.requestMapping);

        settings.data = requestConfiguration.parameters;
        settings.headers = requestConfiguration.headers;

        return settings;
    };

}(this.$t = this.$t || {}));

(function (namespace) {
    "use strict";

    namespace.RestService = function (requestSettings, Request) {
        this.requestOptions = namespace.util.extend({}, this.requestDefaults, requestSettings);
        this.Request = Request;
    };

    var proto = namespace.RestService.prototype;

    proto.requestDefaults = {
        dataType: "json",
        type: "get",
        cache: true,
        crossDomain: true,
        timeout: 20000,
        traditional: true
    };

    proto.process = function (settings) {
        var request = new this.Request(settings);
        request.send();
    };

} (this.$t = this.$t || {}));

﻿if (this.$t)
    (function (ns) {
        "use strict";

        ns.OAuth10a = $t.Class.extend({
            init: function (requestTokenURL, userAuthorizeURL, accessTokenURL, callbackURL, clientID, clientSecret) {
                this.requestTokenURL = requestTokenURL;
                this.userAuthorizeURL = userAuthorizeURL;
                this.accessTokenURL = accessTokenURL;
                this.callbackURL = callbackURL;
                this.clientID = clientID;
                this.clientSecret = clientSecret;
                this.isLogin = false;
                this.timestamp = undefined;
                this.nonce = undefined;
                this.oauth_token = undefined;
                this.error = undefined;
            },
            _generateAcquiring: function () {
                this.timestamp = Math.round(new Date().getTime() / 1000.0);
                this.nonce = Math.random();
                this.nonce = Math.floor(this.nonce * 1000000000);
            },
            _clearTokens: function () {
                this.oauth_token = undefined;
                this.oauth_token_secret = undefined;
            },
            _getTokensFromResponse: function (response) {
                var keyValPairs = response.split("&");
                for (var i = 0; i < keyValPairs.length; i++) {
                    var splits = keyValPairs[i].split("=");
                    switch (splits[0]) {
                        case "oauth_token":
                            this.oauth_token = splits[1];
                            break;
                        case "oauth_token_secret":
                            this.oauth_token_secret = splits[1];
                            break;
                    }
                }
            },
            _sendRequest: function (method, url, authzheaders) {
                try {
                    var request = new XMLHttpRequest();
                    request.open(method, url, false);
                    for (var key in authzheaders)
                        request.setRequestHeader(key, authzheaders[key]);
                    request.send(null);
                    return request;
                } catch (err) {
                    this.error = "Error sending request: " + err;
                    return request;
                }
            },
            _generateKeyForSignature: function () {
                var keyText = this.clientSecret + "&";
                if (this.oauth_token_secret !== undefined)
                    keyText += this.oauth_token_secret;
                return keyText;
            },
            _getSigBaseStringForRequestToken: function () {
                this._generateAcquiring();
                var sigBaseStringParams = "oauth_callback=" + encodeURIComponent(this.callbackURL);
                sigBaseStringParams += "&" + "oauth_consumer_key=" + this.clientID;
                sigBaseStringParams += "&" + "oauth_nonce=" + this.nonce;
                sigBaseStringParams += "&" + "oauth_signature_method=HMAC-SHA1";
                sigBaseStringParams += "&" + "oauth_timestamp=" + this.timestamp;
                sigBaseStringParams += "&" + "oauth_version=1.0";
                var sigBaseString = "POST&";
                sigBaseString += encodeURIComponent(this.requestTokenURL) + "&" + encodeURIComponent(sigBaseStringParams);
                return sigBaseString;
            },
            _getSigBaseStringForAccessToken: function (oauth_verifier) {
                this._generateAcquiring();
                var sigBaseStringParams = "oauth_consumer_key=" + this.clientID;
                sigBaseStringParams += "&" + "oauth_nonce=" + this.nonce;
                sigBaseStringParams += "&" + "oauth_signature_method=HMAC-SHA1";
                sigBaseStringParams += "&" + "oauth_timestamp=" + this.timestamp;
                sigBaseStringParams += "&" + "oauth_token=" + encodeURIComponent(this.oauth_token);
                sigBaseStringParams += "&" + "oauth_verifier=" + encodeURIComponent(oauth_verifier);
                sigBaseStringParams += "&" + "oauth_version=1.0";
                var sigBaseString = "POST&";
                sigBaseString += encodeURIComponent(this.accessTokenURL) + "&" + encodeURIComponent(sigBaseStringParams);
                return sigBaseString;
            },
            _getSigBaseStringForAuthorizedRequests: function (method, url) {
                this._generateAcquiring();
                var sigBaseStringParams = "oauth_consumer_key=" + encodeURIComponent(this.clientID);
                sigBaseStringParams += "&" + "oauth_nonce=" + this.nonce;
                sigBaseStringParams += "&" + "oauth_signature_method=HMAC-SHA1";
                sigBaseStringParams += "&" + "oauth_timestamp=" + this.timestamp;
                sigBaseStringParams += "&" + "oauth_token=" + encodeURIComponent(this.oauth_token);
                sigBaseStringParams += "&" + "oauth_version=1.0";
                var sigBaseString = method + "&";
                sigBaseString += encodeURIComponent(url) + "&" + encodeURIComponent(sigBaseStringParams);
                return sigBaseString;
            },
            login: function () {
                this.error = undefined;
                this._getRequestToken();
            },
            getError: function () {
                return this.error;
            },
            getSecureContext: function () {
                if (!this.isLogin) {
                    return null;
                }
                return {
                    'version': '1.0a',
                    'header': function (url) {
                        var signature = this._getSignature(this._getSigBaseStringForAuthorizedRequests("GET", url), this._generateKeyForSignature());
                        return "OAuth oauth_consumer_key=\"" + this.clientID + "\", oauth_nonce=\"" + this.nonce +
                                    "\", oauth_signature=\"" + encodeURIComponent(this.signature) + "\", oauth_signature_method=\"HMAC-SHA1\"" +
                                    ", oauth_timestamp=\"" + this.timestamp + "\", oauth_token=\"" + this.oauth_token + "\"" +
                                    ", oauth_version=\"1.0\"";
                    }
                }
            }
        });
    }(this.$t.oauth = this.$t.oauth || {}));

﻿if (this.$t)
    (function (ns) {
        "use strict";

        ns.OAuth20 = $t.Class.extend({
            init: function (clientID, authStart, authEnd) {
                this.token = undefined;
                this.error = undefined;
                this.isLogin = false;
                this.clientID = clientID;
                this.authStart = authStart;
                this.authEnd = authEnd;
            },
            _sendRequest: function (method, url, authzheaders) {
                try {
                    var request = new XMLHttpRequest();
                    request.open(method, url, false);
                    for (var key in authzheaders)
                        request.setRequestHeader(key, authzheaders[key]);
                    request.send(null);
                    return request;
                } catch (err) {
                    this.error = "Error sending request: " + err;
                    return request;
                }
            },
            login: function (scope) {
                this.error = undefined;
                this.isLogin = false;
                if (this.localStorage != undefined)
                    this.localStorage.setItem('authorizationClientID', this.clientID);
                this._userAuthorize(scope);
            },
            getSecureContext: function () {
                if (this.isLogin == false)
                    return null;
                return {
                    'version': '2.0',
                    'header': this.token
                }
            },
            getError: function () {
                return this.error;
            }
        });
    }(this.$t.oauth = this.$t.oauth || {}));

/**
 * Implementation of Request for Firefox
 */
(function (namespace) {
    "use strict";

    namespace.Request = function (options) {
        this.options = options;
    };

    namespace.Request.prototype.send = function () {
        var _options = this.options;
        if (_options.type && _options.type !== 'get') {
            if (_options.dataType && _options.dataType.indexOf('xml') !== -1) {
                _options.data = namespace.util.dataToXml(_options.data);
            } else if (_options.dataType && _options.dataType.indexOf('json') !== -1 && JSON && (typeof JSON.stringify === 'function')) {
                _options.data = JSON.stringify(_options.data);
            }
        }
        jQuery.ajax(_options);       
    };

} (this.$t = this.$t || {}));

(function (ns) {
    "use strict";

	ns.initializeProperties = function(pack, extensions) {
	    var keys = Object.keys(extensions);
	    var props;
	    var i = 0, l;
	    for (l = keys.length; i < l; i++) {
	        var key = keys[i];
	        var enumerable = key.charCodeAt(0) !== /*_*/95;
	        var ext = extensions[key];
	        if (ext && typeof ext === 'object') {
	            if (ext.value !== undefined || typeof ext.get === 'function' || typeof ext.set === 'function') {
	                if (ext.enumerable === undefined) {
	                    ext.enumerable = enumerable;
	                }
	                props = props || {};
	                props[key] = ext;
	                continue;
	            }
	        }
	        if (!enumerable) {
	            props = props || {};
	            props[key] = { value: ext, enumerable: enumerable, configurable: true, writable: true }
	            continue;
	        }
	        pack[key] = ext;
	    }
	    if (props) {
	        Object.defineProperties(pack, props);
	    }
	}
	
	ns.Namespace = {};
	ns.Namespace.define = function(name, extensions) {
	    
	    var currentPackage = ns, packageParts = name.split(".");
	
	    for (var i = 0, l = packageParts.length; i < l; i++) {
	        var packageName = packageParts[i];
	        if (!currentPackage[packageName]) {
	            Object.defineProperty(currentPackage, packageName,
	                { value: {}, writable: false, enumerable: true, configurable: true }
	            );
	        }
	        currentPackage = currentPackage[packageName];
	    }
	
	    if (extensions) {
	        ns.initializeProperties(currentPackage, extensions);
	    }
	
	    return currentPackage;
	}

} (this.$t = this.$t || {}));
/**
 * Windows 8 - Preview - List View
 */
(function (ns) {
    "use strict";

    ns.Windows8PreviewListView = function (mapping, data) {
        ns.DefaultMappingRenderer.call(this, mapping, data);
    };

    ns.Windows8PreviewListView.prototype = new ns.DefaultMappingRenderer();
    ns.Windows8PreviewListView.prototype.constructor = new ns.Windows8PreviewListView();
    ns.Windows8PreviewListView.prototype.isSuitable = function (mapping) {
        var component = ns.util.select("#" + mapping.ID);
        return mapping.ID !== "___local_storage___" && component && component.getAttribute("tiggzi-control") === "Windows8.Preview.ListView";
    };

    ns.Windows8PreviewListView.prototype.render = function () {
        var mapping = this.mapping;

        var array = this.getValueFromPath(mapping.PATH, this.data);

        var component = ns.util.select("#" + mapping.ID);

        component.refreshLayout(array.length);

        for (var i = 0; i < array.length; ++i) {
            var item = array[i];
            var itemComponent = component.getItem(i);

            for (var mappingKey in mapping.SET) {
                if (mapping.SET.hasOwnProperty(mappingKey)) {
                    var elementMapping = mapping.SET[mappingKey];
                    var value = this.getValueFromPath(elementMapping.PATH, item);
                    var valueTransformation = elementMapping.TRANSFORMATION;
                    if(valueTransformation) {
                        value = valueTransformation(value);
                    }
                    if (elementMapping.ATTR == "@") {
                        itemComponent.find("#" + elementMapping.ID).append(value);
                    } else {
                        itemComponent.find("#" + elementMapping.ID).attr(elementMapping.ATTR, value);
                    }
                }
            }

            component.refreshItemLayout(i);
        }
    };

    ns.addMappingRendererType(ns.Windows8PreviewListView);

}(this.$t = this.$t || {}));

(function (ns) {
    ns.WinRTStyleEmulation = ns.WinRTStyleEmulation || {};

    var rowAlignMap = {
        start: "top",
        end: "bottom",
        center: "middle"
        // TODO add stretch
    }

    var colAlignMap = {
        start: "left",
        end: "right",
        center: "center",
        stretch: "justify"
    }

    var flexRowAlignMap = {
        "start": "top",
        "end": "bottom",
        "center": "middle",
        "baseline": "baseline"
        //TODO add stretch
    };

    var stretchWidthValues = {
        "metroinput": 250,
        "metrolabel": "getTextWidth",
        "metroheader": "getTextWidth",
        "metroimage": 100,
        "metrobutton": 75,
        "metrolistviewtemplate": 500
    }

    var stretchHeightValues = {
        "metroinput": 25,
        "metrolabel": 25,
        "metroheader": 25,
        "metroimage": 100,
        "metrobutton": 25,
        "metrolistviewtemplate": 500
    }

    var elementsToResize = ["metroheader", "metrolabel", "metrolink"];
    var firstNode = true;

    ns.WinRTStyleEmulation.create_model = function (model, tag) {
        if (firstNode && $(tag).attr("data-tiggzi-component")) {
            firstNode = false;
            create_model_element(model, tag);
        } else {
            firstNode = false;
            $(tag).children().each(function () {
                create_model_element(model, this);
            });
        }
    }

    var create_model_element = function (model, element) {
        var elemId = $(element).attr("id");
        var elemType = $(element).attr("data-tiggzi-component");
        var elemProps = $(element).attr("data-tiggzi-component-props");
        var elemDisplay = $(element).css("display");

        var item = {
            "id": elemId,
            "type": elemType,
            "display": elemDisplay,
            "parent": $(element).parent(),
            "child": $(element).children()
        }
        if (elemType) {
            if (elemProps) {
                var component_props_splitted = elemProps.split(';');
                for (i = 0; i < component_props_splitted.length; i++) {
                    var keyvalues = component_props_splitted[i].split(':');
                    if (keyvalues[1]) {
                        item[$.trim(keyvalues[0])] = $.trim(keyvalues[1]);
                    }
                }
            }
            setMargin(element, item);
            setPadding(element, item);

            if (model[item.id] == undefined) {
                model[item.id] = item;
            }
        }
        if (elemType == "metroflexibleboxitem") {
            updateFlexboxItemModel(element, model[item.id]);
        }

        if (model[item.id]) {
            ns.WinRTStyleEmulation.create_model(model[item.id], $(element));
        } else {
            ns.WinRTStyleEmulation.create_model(model, $(element));
        }
    }

    var setMargin = function (element, item) {
        if (item.margin) {
            $(element).css("margin", item.margin);
        }
        var elemMargins = {};
        elemMargins.top = parseInt($(element).css("margin-top"));
        elemMargins.bottom = parseInt($(element).css("margin-bottom"));
        elemMargins.left = parseInt($(element).css("margin-left"));
        elemMargins.right = parseInt($(element).css("margin-right"));

        item.margin = elemMargins;
    }

    var setPadding = function (element, item) {
        if (item.padding) {
            $(element).css("padding", item.padding);
        }
        var elemPadding = {};
        elemPadding.top = parseInt($(element).css("padding-top"));
        elemPadding.bottom = parseInt($(element).css("padding-bottom"));
        elemPadding.left = parseInt($(element).css("padding-left"));
        elemPadding.right = parseInt($(element).css("padding-right"));

        item.padding = elemPadding;
    }

    var updateFlexboxItemModel = function (element, model) {
        $(element).css("display", "inline-block");
        model.size = $(element).width();

        if (model.flex) {
            var params = model.flex.split(" ");
            model.flex = {
                "positiveFlex": params[0],
                "negativeFlex": params[1],
                "preferredSize": params[2]
            }
        }
    }

    var processItem = function (node, model, context) {
        ns.WinRTStyleEmulation.apply_model(model, context);
    }


    var processScreen = function (node, model, context) {
        processItem(node, model, context);
    };

    var processGrid = function (node, model, context) {
        var parentWidth = model.parent.width();
        var parentHeight = model.parent.height();

        if (model.width && model.width != "stretch") {
            parentWidth = parseInt(model.width);
        }
        if (model.height && model.height != "stretch") {
            parentHeight = parseInt(model.height);
        }

        var childsWidth = calculateGridSize(
            node,
            model.cols,
            model, parentWidth - model.margin.left - model.margin.right - model.padding.left - model.padding.right,
            "width");
        var childsHeight = calculateGridSize(node,
            model.rows,
            model, parentHeight - model.margin.top - model.margin.bottom - model.padding.left - model.padding.right,
            "height");

        var index = 0;
        node.children().filter("colgroup").children().each(function () {
            $(this).width(childsWidth[index]);
            index++;
        });

        var borderSpacing = node.css("border-spacing").split(' ');
        if (borderSpacing.length == 2) {
            borderSpacing = borderSpacing[1];
        }
        index = 0;
        node.children().filter("tbody").children().each(function () {
            var currentHeight = parseInt(childsHeight[index]) - parseInt(borderSpacing) * 2;
            node.find($(this)).height(currentHeight);
            node.find($(this)).children().each(function () {
                $(this).height(currentHeight - parseInt($(this).css("border-width")) * 2 - parseInt($(this).css("padding")) * 2 - parseInt($(this).css("margin")) * 2);
            });
            index++;
        });

        //appendGrid(node, model, context);

        ns.WinRTStyleEmulation.apply_model(model, context);
    };

    var processSettingsFlyout = function (node, model, context) {
        var parentHeight = model.parent.height();

        if (model.height && model.height != "stretch") {
            parentHeight = parseInt(model.height);
        }

        var settingsFlyoutHeaderDims = {
                width: "100%",
                height: "100px"
            },
            settingsFlyoutContentDims = {
                width: "100%",
                height: parentHeight - parseInt(settingsFlyoutHeaderDims.height, 10) + "px"
            };
        var settingsFlyoutItems = node.children();

        // we have only 2 item in setting flyout item: header and content items
        if (settingsFlyoutItems && settingsFlyoutItems.length === 2) {
            $(settingsFlyoutItems[0]).css(settingsFlyoutHeaderDims);
            $(settingsFlyoutItems[1]).css(settingsFlyoutContentDims);
        }

        ns.WinRTStyleEmulation.apply_model(model, context);
    };

    var processCell = function (node, model, context) {
        node.attr("valign", rowAlignMap[model.rowAlign]);
        node.attr("align", colAlignMap[model.colAlign]);
        node.attr("rowspan", model.rowSpan);
        node.attr("colspan", model.colSpan);

        ns.WinRTStyleEmulation.apply_model(model, context);
    };

    var processListView = function (node, model, context) {
        setSizes(node, model);
        var id = node.attr("id");
        $("#" + id + "_viewport").css("width", $("#" + id).width() + "px");
        $("#" + id + "_viewport").css("height", $("#" + id).height() + "px");
        ns.WinRTStyleEmulation.apply_model(model, context);
    }

    var processListViewTemplate = function (node, model, context) {
        setSizes(node, model);
        ns.WinRTStyleEmulation.apply_model(model, context);
    }

    var processFlexbox = function (node, model, context) {
        var maxHeight = 0;
        var usedSpace = 0;
        var wrapRows = new Array();

        var parentSize = flexboxParentSize(node, model, context);
        updateFlexboxItemSize(node, model, parentSize);

        if (model.direction == "row") {
            node.css("display", "table-cell");
            node.css("vertical-align", flexRowAlignMap[model.flexAlign]);

            if (model.flexWrap == "wrap") {
                $.each(model, function (key, value) {
                    if (value.type == "metroflexibleboxitem") {

                        var flexBoxItem = node.find("#" + value.id);
                        flexBoxItem.css("float", "left");

                        usedSpace += value.size;
                        if (usedSpace <= parentSize) {
                            wrapRows.push(value.id);
                        } else {
                            usedSpace -= value.size;
                            calculateFlexWrap(node, model, parentSize, usedSpace, wrapRows);
                            wrapRows = new Array();
                            wrapRows.push(value.id);

                            flexBoxItem.before("<div id='none" + value.id + "'></div>");
                            node.find("#none" + value.id).css("height", maxHeight);

                            usedSpace = value.size;
                            maxHeight = 0;
                        }
                        if (maxHeight < flexBoxItem.height()) {
                            maxHeight = flexBoxItem.height();
                        }
                    }
                });
                if (wrapRows.length > 0) {
                    calculateFlexWrap(node, model, parentSize, usedSpace, wrapRows);
                }
            } else {
                calculateFlexBoxItemSize(node, model, parentSize);
            }
        }
        ns.WinRTStyleEmulation.apply_model(model, context);
    }

    var processFlexboxItem = function (node, model, context) {
        ns.WinRTStyleEmulation.apply_model(model, context);
    }

    var setSizes = function (node, model) {
        var parentBorder = parseInt(model.parent.css("border-width")) * 2;
        var parentPaddingLeftRight = parseInt(model.parent.css("padding-left")) + parseInt(model.parent.css("padding-right"));
        var parentPaddingTopBottom = parseInt(model.parent.css("padding-top")) + parseInt(model.parent.css("padding-bottom"));
        var borderSpacing = model.parent.css("border-spacing").split(' ');
        if (borderSpacing.length == 2) {
            borderSpacing = borderSpacing[1];
        }

        var parentWidth = model.parent.width() - parentPaddingLeftRight - parentBorder;
        var parentHeight = model.parent.height() - parentPaddingTopBottom - parentBorder;

        if (model.width) {
            if (model.width.indexOf("px") != -1) {
                var width = parseInt(model.width, 10) +
                    model.margin.left +
                    model.margin.right;
            }
            if (model.width == "stretch") {
                var width = parentWidth - model.margin.left - model.margin.right;
            }

            if (width < parentWidth) {
                node.css("width", width);
            } else if (width > 0) {
                node.css("width", parentWidth - model.margin.left - model.margin.right);
            }
        }

        if (model.height) {
            if (model.height.indexOf("px") != -1) {
                var height = parseInt(model.height, 10) +
                    model.margin.top +
                    model.margin.bottom;
            }
            if (model.height == "stretch") {
                var height = parentHeight - model.margin.top - model.margin.bottom - parseInt(borderSpacing) * 2;
            }

            if (height < parentHeight) {
                node.css("height", height);
            } else if (height > 0) {
                node.css("height", parentHeight - model.margin.top - model.margin.bottom - parseInt(borderSpacing) * 2);
            }
        }
    }

    var outMapper = {
        'metroscreen': processScreen,
        'metrogrid': processGrid,
        'metrogridcell': processCell,
        'metrolistview': processListView,
        'metrolistviewtemplate': processListViewTemplate,
        'metroflexiblebox': processFlexbox,
        'metroflexibleboxitem': processFlexboxItem,
        'metroinput': setSizes,
        'metroheader': setSizes,
        'metrolabel': setSizes,
        'metrolink': setSizes,
        'metrobutton': setSizes,
        'metroimage': setSizes,
        'metrosettingsflyout': processSettingsFlyout,
        'metrosettingsflyoutitem': processItem
    }

    ns.WinRTStyleEmulation.apply_model = function (model, context) {
        firstNode = true;
        $.each(model, function (key, value) {
            if (outMapper[value.type]) {
                outMapper[value.type]($("#" + key, context), model[key], context);
            }
        });
    }

    var appendGrid = function (node, model, context) {
        var generatedTd = node.find("#emptytd_" + model.id).hide();
        var generatedTr = node.find("#emptytr_" + model.id).hide();
        var gridWidth = node.outerWidth(true);
        var gridHeight = node.outerHeight(true);

        if (model.width) {
            var availableWidth;
            if (model.width == "stretch") {
                availableWidth = model.parent.width() - gridWidth;
            } else {
                availableWidth = parseInt(model.width) - gridWidth;
            }
            if (availableWidth > 0) {
                if (generatedTd.length == 0) {
                    $('> tbody > tr:last > td:last', node).after("<td id='emptytd_" + model.id + "'></td>");
                    generatedTd = node.find("#emptytd_" + model.id);
                }
                generatedTd.css("width", availableWidth);
            }
            generatedTd.show();
        }

        if (model.height) {
            var availableHeight;
            if (model.height == "stretch") {
                availableHeight = model.parent.height() - model.margin.top - model.margin.bottom - gridHeight;
            } else {
                availableHeight = parseInt(model.height) - model.margin.top - model.margin.bottom - gridHeight;
            }
            if (availableHeight > 0) {
                if (generatedTr.length == 0) {
                    $('> tbody > tr:last', node).after("<tr id='emptytr_" + model.id + "'></tr>");
                    generatedTr = node.find("#emptytr_" + model.id);
                }
                generatedTr.css("height", availableHeight);
            }
            generatedTr.show();
        }
    }

    var flexboxParentSize = function (node, model, context) {
        var parentWidth = model.parent.width();
        var parentHeight = model.parent.height();

        if (model.width && model.width != "stretch") {
            parentWidth = parseInt(model.width);
        }
        if (model.height && model.height != "stretch") {
            parentHeight = parseInt(model.height);
        }

        if (model.direction == "row") {
            var parentSize = parentWidth;
        } else {
            var parentSize = parentHeight;
        }
        return parentSize;
    }

    var updateFlexboxItemSize = function (node, model, parentSize) {
        $.each(model, function (key, value) {
            if (value.type == "metroflexibleboxitem") {
                if (value.flex) {
                    if (value.flex.preferredSize.indexOf("px") != -1) {
                        if (parseInt(value.flex.preferredSize) > value.size) {
                            value.size = parseInt(value.flex.preferredSize);
                        }
                    }
                    if (value.flex.preferredSize.indexOf("auto") != -1) {
                        var child = value[value.child];
                        if (child.width && child.width.indexOf("px") != -1) {
                            autoSize = parseInt(child.width) +
                                child.margin.left +
                                child.margin.right;
                        } else {
                            autoSize = value.child.width() +
                                child.margin.left +
                                child.margin.right;
                        }
                        if (autoSize > value.size) {
                            value.size = autoSize;
                        }
                    }
                }
            }
        });
    }

    var calculateFlexWrap = function (node, model, parentSize, usedSpace, wrapRows) {
        var positiveFlex = 0;
        for (var i = 0; i < wrapRows.length; i++) {
            if (model[wrapRows[i]].flex) {
                positiveFlex += parseInt(model[wrapRows[i]].flex.positiveFlex);
            }
        }
        var availableSpace = parentSize - usedSpace;
        for (var i = 0; i < wrapRows.length; i++) {
            if (model[wrapRows[i]].flex) {
                var spaceToAdd = availableSpace / positiveFlex;
                node.find("#" + model[wrapRows[i]].id).css("width", model[wrapRows[i]].size + model[wrapRows[i]].flex.positiveFlex * spaceToAdd);
            }
        }
    }

    var calculateFlexBoxItemSize = function (node, model, parentSize) {
        var usedSpace = 0;
        var positiveFlex = 0;
        var negativeFlex = 0;

        $.each(model, function (key, value) {
            if (value.type == "metroflexibleboxitem") {
                node.find("#" + value.id).css("float", "left");
                if (value.flex) {
                    positiveFlex += parseInt(value.flex.positiveFlex);
                    negativeFlex += parseInt(value.flex.negativeFlex);
                }
                if (value.size >= 0) {
                    usedSpace += value.size;
                }
            }
        });
        var availableSpace = parentSize - usedSpace;
        $.each(model, function (key, value) {
            if (value.type == "metroflexibleboxitem") {
                if (value.flex) {
                    if (availableSpace > 0) {
                        var spaceToAdd = availableSpace / positiveFlex;
                        node.find("#" + value.id).css("width", value.size + value.flex.positiveFlex * spaceToAdd);
                    } else {
                        var spaceToAdd = availableSpace / negativeFlex;
                        node.find("#" + value.id).css("width", value.size + value.flex.negativeFlex * spaceToAdd);
                    }
                }
            }
        });
    }

    function calculateGridSize(node, values, model, parentSize, type) {
        var params = values.split(" ");
        var result = new Array();
        var frValues = new Array();
        var frCount = 0;
        var frSize = 0;

        for (i = 0; i < params.length; i++) {
            if (params[i].indexOf("px") != -1) {
                result.push(params[i]);
            } else if (params[i].indexOf("auto") != -1) {
                var autoSize = calculateAuto(node, i + 1, model, type);
                result.push(autoSize);
            } else if (params[i].indexOf("fr") != -1) {
                frCount += parseInt(params[i]);
                result.push(-1);
                frValues.push(parseInt(params[i]));
            }
        }
        var unavailable = 0;
        for (i = 0; i < result.length; i++) {
            if (result[i] != -1) {
                unavailable += parseInt(result[i]);
            }
        }
        frSize = (parentSize - unavailable) / frCount;
        for (i = 0; i < frCount; i++) {
            result[result.indexOf(-1)] = frSize * frValues[i] + "px";
        }
        return result;
    }

    function calculateAuto(node, index, model, type) {
        var maxsize = 0;

        $.each(model, function (key, value) {
            if (value.child && value.child.length > 0) {
                if (type == "width") {
                    if (value.col == index) {
                        var childElem = value[value.child.attr("id")];
                        if (childElem.display != "none" && childElem.width) {
                            if (childElem.width.indexOf("px") != -1) {
                                var width = parseInt(childElem.width, 10) +
                                    childElem.margin.left +
                                    childElem.margin.right;
                            }
                            if (childElem.width == "stretch" && stretchWidthValues[childElem.type]) {
                                var width = stretchWidthValues[childElem.type];

                                if (width == "getTextWidth") {
                                    width = value.child.outerWidth(true);

                                    if ($.inArray(childElem.type, elementsToResize) != -1) {
                                        if (value.child.width() > 0) {
                                            value.child.css("width", value.child.width());
                                        }
                                    }
                                }
                            }
                            if (maxsize < width && value.colSpan == 1) {
                                maxsize = width;
                            }
                        } else {
                            var width = value.child.outerWidth(true);

                            if ($.inArray(childElem.type, elementsToResize) != -1) {
                                if (value.child.width() > 0) {
                                    value.child.css("width", value.child.width());
                                }
                            }
                            if (maxsize < width && value.colSpan == 1) {
                                maxsize = width;
                            }
                        }
                    }
                } else {
                    if (value.row == index) {
                        var childElem = value[value.child.attr("id")];
                        if (childElem.display != "none" && childElem.height) {
                            if (childElem.height.indexOf("px") != -1) {
                                var height = parseInt(childElem.height, 10) +
                                    childElem.margin.top +
                                    childElem.margin.bottom;
                            }

                            if (childElem.height == "stretch" && stretchHeightValues[childElem.type]) {
                                var height = stretchHeightValues[childElem.type];
                            }
                            if (maxsize < height && value.rowSpan == 1) {
                                maxsize = height;
                            }
                        } else {
                            var height = value.child.outerHeight(true);
                            if (maxsize < height && value.rowSpan == 1) {
                                maxsize = height;
                            }
                        }
                    }
                }
            }
        });
        return maxsize + "px";
    }

}(this.$t = this.$t || {}));

﻿if (this.$t)
    (function (ns) {
        "use strict";

        var oauthVerifierIndex = location.href.indexOf("oauth_verifier")
        if (oauthVerifierIndex != -1)
            ns.oauthVerifier = location.href.substring(location.href.indexOf("=", oauthVerifierIndex) + 1, location.href.indexOf("&", oauthVerifierIndex) > 0 ? location.href.indexOf("&", oauthVerifierIndex) : location.href.lenght)
        var oauthTokenIndex = location.href.indexOf("oauth_token")
        if (oauthTokenIndex != -1)
            ns.oauthToken = location.href.substring(location.href.indexOf("=", oauthTokenIndex) + 1, location.href.indexOf("&", oauthTokenIndex) > 0 ? location.href.indexOf("&", oauthTokenIndex) : location.href.lenght)

        ns.OAuth10aPreview = $t.oauth.OAuth10a.extend({
            init: function (requestTokenURL, userAuthorizeURL, accessTokenURL, callbackURL, clientID, clientSecret, localStorage) {
                this._super(requestTokenURL, userAuthorizeURL, accessTokenURL, callbackURL, clientID, clientSecret);
                this.localStorage = localStorage;
                this.tunnelURL = "http://project.tiggzi.local/rest/plaintunnel";

                if (localStorage != undefined &&
                localStorage.getItem('authorizationClientID') == clientID &&
                $t.oauth.preview != undefined &&
                $t.oauth.preview.oauthToken != undefined &&
                $t.oauth.preview.oauthVerifier != undefined) {
                    localStorage.removeItem('authorizationClientID');
                    this.oauth_token_secret = localStorage.getItem('oauth_token_secret');
                    localStorage.removeItem('oauth_token_secret');
                    this._getAccessToken($t.oauth.preview.oauthVerifier, $t.oauth.preview.oauthToken);
                }
            },
            //section 6.1 of http://oauth.net/core/1.0a/
            _getRequestToken: function () {
                var signature = this._getSignature(this._getSigBaseStringForRequestToken(), this._generateKeyForSignature());
                var header = {
                    "tiggzi-remote-host-url": this.requestTokenURL,
                    "Content-Type" : "application/x-www-form-urlencoded"
                };
                header["Authorization"] = "OAuth oauth_callback=\"" + encodeURIComponent(this.callbackURL) + "\", oauth_consumer_key=\"" + this.clientID +
                    "\", oauth_nonce=\"" + this.nonce + "\", oauth_signature_method=\"HMAC-SHA1\", oauth_timestamp=\"" +
                    this.timestamp + "\", oauth_version=\"1.0\", oauth_signature=\"" + encodeURIComponent(signature) + "\"";
                var response = this._sendRequest("POST", this.tunnelURL, header);
                if (response.status != 200) {
                    if (!this.error)
                        this.error = response.responseText;
                    this._clearTokens();
                    this.isLogin = false;
                    return;
                }
                this._getTokensFromResponse(response.responseText);
                if (this.localStorage != undefined) {
                    this.localStorage.setItem('authorizationClientID', this.clientID);
                    this.localStorage.setItem('oauth_token_secret', this.oauth_token_secret);
                }
                this._userAuthorize();
            },
            //section 6.2 of http://oauth.net/core/1.0a/
            _userAuthorize: function () {
                var startURI = this.userAuthorizeURL + "?oauth_token=" + this.oauth_token;
                location = startURI;
            },
            //section 6.3 of http://oauth.net/core/1.0a/
            _getAccessToken: function (oauth_verifier, oauth_token) {
                if (oauth_token != undefined)
                    this.oauth_token = oauth_token;
                var signature = this._getSignature(this._getSigBaseStringForAccessToken(oauth_verifier), this._generateKeyForSignature());
                var header = {
                    "tiggzi-remote-host-url": this.accessTokenURL,
                    "Content-Type": "application/x-www-form-urlencoded"
                };
                header["Authorization"] = "OAuth oauth_consumer_key=\"" + this.clientID + "\", oauth_nonce=\"" + this.nonce +
                    "\", oauth_signature_method=\"HMAC-SHA1\", oauth_timestamp=\"" + this.timestamp +
                    "\", oauth_version=\"1.0\", oauth_signature=\"" + encodeURIComponent(signature) +
                    "\"" + ", oauth_token=\"" + encodeURIComponent(this.oauth_token) + "\"" + ", oauth_verifier=\"" + oauth_verifier + "\"";
                var response = this._sendRequest("POST", this.tunnelURL, header);
                if (response.status != 200) {
                    if (!this.error)
                        this.error = response.responseText;
                    this._clearTokens();
                    this.isLogin = false;
                    return;
                }
                this._getTokensFromResponse(response.responseText);
                this.isLogin = true;
            },
            _getSignature: function (sigBaseString, keyText) {
                $t.util.b64pad = '=';
                return $t.util.b64_hmac_sha1(keyText, sigBaseString);
            },
            getAccountInfo: function (url, headers) {
                if (!this.isLogin) {
                    return {};
                }
                var signature = this._getSignature(this._getSigBaseStringForAuthorizedRequests("GET", url), this._generateKeyForSignature());
                headers["tiggzi-remote-host-url"] = url;
                headers["Content-Type"] = "application/x-www-form-urlencoded";
                headers["Authorization"] = "OAuth oauth_consumer_key=\"" + this.clientID + "\", oauth_nonce=\"" + this.nonce +
							"\", oauth_signature=\"" + encodeURIComponent(signature) + "\", oauth_signature_method=\"HMAC-SHA1\"" +
							", oauth_timestamp=\"" + this.timestamp + "\", oauth_token=\"" + this.oauth_token + "\"" +
							", oauth_version=\"1.0\"";
                var response = this._sendRequest("GET", this.tunnelURL, headers);
                if (response.status != 200) {
                    return {};
                }
                else {
                    return response.responseText;
                }
            }
        });
    }(this.$t.oauth.preview = this.$t.oauth.preview || {}));

﻿if (this.$t)
    (function (ns) {
        "use strict";

        var accessTokenIndex = location.href.indexOf("access_token")
        if (accessTokenIndex != -1)
            ns.accessToken = location.href.substring(location.href.indexOf("=", accessTokenIndex) + 1, location.href.indexOf("&", accessTokenIndex) > 0 ? location.href.indexOf("&", accessTokenIndex) : location.href.lenght)

        ns.OAuth20Preview = $t.oauth.OAuth20.extend({
            init: function (clientID, authStart, authEnd, localStorage) {
                this._super(clientID, authStart, authEnd);
                this.localStorage = localStorage;
                this.tunnelURL = "http://project.tiggzi.local/rest/plaintunnel";
                this._checkAccessToken();
            },
            _checkAccessToken: function() {
                if (localStorage != undefined &&
                    localStorage.getItem('authorizationClientID') == this.clientID &&
                    $t.oauth.preview != undefined &&
                    $t.oauth.preview.accessToken != undefined) {
                    this.token = $t.oauth.preview.accessToken;
                    this.isLogin = true;
                    localStorage.removeItem('authorizationClientID');
                }
            },
            _userAuthorize: function (scope) {
                var url = this.authStart;
                url += "?client_id=" + this.clientID + "&redirect_uri=" + encodeURIComponent(this.authEnd) + "&scope=" + scope + "&response_type=token";
                location = url;
            },
            getAccountInfo: function (url) {
                if (!this.isLogin) {
                    return {};
                }
                var headers = {};
                var params = "?access_token=" + encodeURIComponent(this.getSecureContext().header);
                headers["tiggzi-remote-host-url"] = url + params;
                headers["Content-Type"] = "application/x-www-form-urlencoded";
                var response = this._sendRequest("GET", this.tunnelURL, headers);
                if (response.status != 200) {
                    return {};
                }
                else {
                    return response.responseText;
                }
            }
        });
    }(this.$t.oauth.preview = this.$t.oauth.preview || {}));

﻿if (this.$t)
    (function (ns) {
        "use strict";

        ns.OAuthGooglePreview = $t.oauth.preview.OAuth20Preview.extend({
            init: function (clientID, authEnd, localStorage) {
                this._super(clientID,
                    "https://accounts.google.com/o/oauth2/auth",
                    authEnd,
                    localStorage);
                this.tunnelURL = "http://project.tiggzi.local/rest/plaintunnel";
                this.googleToken = "https://www.googleapis.com/oauth2/v1/tokeninfo";
                this._checkAccessToken();
            },
            _validateLogin: function (token) {
                this.token = token;
                var url = this.googleToken + "?access_token=" + encodeURIComponent(this.token);
                var headers = {};
                headers["tiggzi-remote-host-url"] = url;
                headers["Content-Type"] = "application/x-www-form-urlencoded";
                var response = this._sendRequest("GET", this.tunnelURL, headers);
                if (response.status != 200) {
                    if (!error)
                        this.error = response.responseText;
                    this.clearTokens();
                    this.isLogin = false;
                    return;
                }
                var resultJSON = eval('(' + response.responseText + ')');
                if (resultJSON.error == undefined)
                    this.isLogin = true;
                else
                    this.isLogin = false;
            }
        });
    }(this.$t.oauth.preview = this.$t.oauth.preview || {}));

